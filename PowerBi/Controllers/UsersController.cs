﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models.Security;
using Core.Models.User;
using Core.Models;
using TarjetasColpatria.ViewModel;
using PagedList;


namespace PowerBi.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class UsersController : Controller
    {
        private Contexto db = new Contexto();

        /*------------------------------------------------------*
        * ADMINISTRACION DE USUARIOS
        *------------------------------------------------------*/

        // GET: Users
        public ActionResult Index(List<string> camp, string documento, string nombre, int? i)
        {           
          

            if (!string.IsNullOrEmpty(documento) || !string.IsNullOrEmpty(nombre))
            {
                var users = db.Usuario.Where(x => x.Document.Trim().Contains(documento.Trim()) || x.Names.Trim().Contains(nombre.Trim())).ToList();

                ViewBag.Campana = new SelectList(db.campanas, "Id", "Nombre");
                ViewBag.Dashboard = new SelectList(db.Dashboard, "Id", "Nombre");
                ViewBag.Reportes = new SelectList(db.Reportes, "Id", "Nombre");

                ViewBag.users = users.ToList().ToPagedList(i ?? 1, 10);

                return View();


            }
            else
            {
                var users = db.Usuario.ToList();

                ViewBag.Campana = new SelectList(db.campanas, "Id", "Nombre");
                ViewBag.Dashboard = new SelectList(db.Dashboard, "Id", "Nombre");
                ViewBag.Reportes = new SelectList(db.Reportes, "Id", "Nombre");

                ViewBag.users = users.ToList().ToPagedList(i ?? 1, 10);

                return View();


            }


        }

        public JsonResult GetDashboards(Guid Id)
        {
            var dash = db.Dashboard.Where(c => c.CampanaId == Id);
            return Json(dash);
        }

        public JsonResult GetReportes(Guid Id)
        {
            var Reports = db.Reportes.Where(c => c.DashboardId == Id);
            return Json(Reports);
        }


        public JsonResult GetPermisos(Guid? Id)
        {
            var Permisos = db.permisosdash.Include(c => c.PermisosuserId).Include(c => c.dashboardId).Include(c => c.campanaId).Include(c => c.reportesId).Where(c => c.PermisosUsers == Id);
            return Json(Permisos);
        }



        // GET: Users/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Document,Names,LastName,Phone1,Phone2,Phone3,Email,Status,PassWord,login,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Id = Guid.NewGuid();
                db.Usuario.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Document,Names,LastName,Phone1,Phone2,Phone3,Email,Status,PassWord,login,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            User user = db.Usuario.Find(id);
            db.Usuario.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*------------------------------------------------------*
        * ADMINISTRACION DE ROLES
        *------------------------------------------------------*/
        
        public ActionResult Roles(Guid Id)
        {
            User Usuario = db.Usuario.Find(Id);
            if (Id == null || Usuario == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Name = Usuario.Names;      
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UsuariosRoles.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Roles, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Roles([Bind(Include = "Id,RolId,UserId")] UserRol userRol, string Name)
        {
            if (ModelState.IsValid)
            {
                var Exist = db.UsuariosRoles
                    .Where(
                    c => c.UserId == userRol.UserId&&
                    c.RolId == userRol.RolId
                ).FirstOrDefault();

                if (Exist != null)
                {
                    ModelState.AddModelError("RolId", "este permiso ya fue asignado");
                }
                else
                {
                    userRol.Id = Guid.NewGuid();
                    db.UsuariosRoles.Add(userRol);
                    db.SaveChanges();
                    return RedirectToAction("Roles", new { Id = userRol.UserId});
                }


            }
            var Id = userRol.UserId;
            ViewBag.Name = Name;
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UsuariosRoles.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Roles, "Id", "Name");
            return View("Roles");
        }

        // POST: UserOfPointOfCares/Delete/5
        [HttpPost, ActionName("DeleteRol")]
        [ValidateAntiForgeryToken]
        public ActionResult RolesDeleteConfirmed(Guid id)
        {
            UserRol UsusarioRol = db.UsuariosRoles.Find(id);
            var UserId = UsusarioRol.UserId;
            db.UsuariosRoles.Remove(UsusarioRol);
            db.SaveChanges();
            return RedirectToAction("Roles", new { Id = UserId });
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
