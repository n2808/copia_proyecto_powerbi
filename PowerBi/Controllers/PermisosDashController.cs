﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Models.Security;
using Core.Models.User;
using Microsoft.PowerBI.Api.V2;
using Microsoft.PowerBI.Api.V2.Models;
using TarjetasColpatria.Models.Permisos;

namespace TarjetasColpatria.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class PermisosDashController : Controller
    {
        private Contexto db = new Contexto();

        // GET: PermisosDash
        public ActionResult Index()
        {
            var permisosdash = db.permisosdash.Include(p => p.campanaId).Include(p => p.dashboardId).Include(p => p.reportesId);
            return View(permisosdash.ToList());
        }


        public JsonResult AsignarPermiso(PermisosDash permisosdash)
        {
            try
            {
                db.permisosdash.Add(permisosdash);
                db.SaveChanges();

                var Permisos = db.permisosdash.Include(c => c.PermisosuserId).Include(c => c.dashboardId).Include(c => c.campanaId).Include(c => c.reportesId).Where(c => c.PermisosUsers == permisosdash.PermisosUsers);

                return Json(new { status = 200,message = Permisos });
            }
            catch (Exception ex)
            {
                return Json(new { status = 400,message = ex.InnerException.Message });
                
            }

            

        }

        // GET: PermisosDash
        public ActionResult IndexUsers(User Id)
        {
            User user = db.Usuario.Find(Id);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Name = user.Names;
            ViewBag.Id = Id;
            ViewBag.permisosdash = db.permisosdash.Include(s => s.PermisosUsers).Where(s => s.PermisosuserId == Id);
            return View();
        }


        // GET: PermisosDash/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PermisosDash permisosDash = db.permisosdash.Find(id);
            if (permisosDash == null)
            {
                return HttpNotFound();
            }
            return View(permisosDash);
        }

        // GET: PermisosDash/Create
        public ActionResult Create(List<string> Permisos)
        {



            ViewBag.Campana = new SelectList(db.campanas.OrderBy(c => c.Nombre), "Id", "Nombre");
           // ViewBag.Dashboard = new SelectList(db.Dashboard.Where(c => c.CampanaId == db.campanas.FirstOrDefault().Id));
            //ViewBag.Dashboard = new SelectList(db.Dashboard.Where(c => c.Estado == true).Select(c => new { Id = c.Id, Nombre = c.Nombre }),"Id","Nombre",Dashboard);
           // ViewBag.dash = ViewBag.Dashobard;
            ViewBag.Dashboard = new SelectList(db.Dashboard, "Id", "Nombre");
            ViewBag.PermisoCampana = new SelectList(db.campanas, "Id", "Nombre");
            ViewBag.PermisoDashboard = new SelectList(db.Dashboard, "Id", "Nombre");
            ViewBag.Permisos = new SelectList(db.Reportes.Where(c => c.Estado == true).Select(c => new { c.Id, nombre = c.Nombre }).OrderBy(x => x.nombre), "Id", "Nombre",Permisos);

            ViewBag.Permisos = db.Reportes.Where(c => c.Estado == true).Select(c => new Custom { Id = c.Id, nombre = c.Nombre }).OrderBy(x => x.nombre).ToList();
            ViewBag.Report = Permisos;
            ViewBag.Reportes = new SelectList(db.Reportes, "Id", "Nombre");
            return View();
        }

       public JsonResult GetDashboards(Guid Id)
        {
            var dash = db.Dashboard.Where(c => c.CampanaId == Id);
            return Json(dash);
        }

        public JsonResult GetReportes(Guid Id)
        {
            var Reports = db.Reportes.Where(c => c.DashboardId == Id);
            return Json(Reports);
        }


        // POST: PermisosDash/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Campana,Dashboard,Reportes,Permisos,PermisoTipo,PermisoCampana,PermisoDashboard,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] PermisosDash permisosDash)
        {
            if (ModelState.IsValid)
            {
                permisosDash.Id = Guid.NewGuid();
                db.permisosdash.Add(permisosDash);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Campana = new SelectList(db.campanas, "Id", "Nombre", permisosDash.Campana);
            ViewBag.Dashboard = new SelectList(db.Dashboard, "Id", "Nombre", permisosDash.Dashboard);
            //ViewBag.PermisoCampana = new SelectList(db.campanas, "Id", "Nombre", permisosDash.PermisoCampana);
            //ViewBag.PermisoDashboard = new SelectList(db.Dashboard, "Id", "Nombre", permisosDash.PermisoDashboard);
            ViewBag.Permisos = new SelectList(db.Reportes, "Id", "Nombre", permisosDash.Reportes);
            ViewBag.Reportes = new SelectList(db.Reportes, "Id", "Nombre", permisosDash.Reportes);
            return View(permisosDash);
        }

        // GET: PermisosDash/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PermisosDash permisosDash = db.permisosdash.Find(id);
            if (permisosDash == null)
            {
                return HttpNotFound();
            }
            ViewBag.Campana = new SelectList(db.campanas, "Id", "Nombre", permisosDash.Campana);
            ViewBag.Dashboard = new SelectList(db.Dashboard, "Id", "Nombre", permisosDash.Dashboard);
            //ViewBag.PermisoCampana = new SelectList(db.campanas, "Id", "Nombre", permisosDash.PermisoCampana);
            //ViewBag.PermisoDashboard = new SelectList(db.Dashboard, "Id", "Nombre", permisosDash.PermisoDashboard);
            ViewBag.Permisos = new SelectList(db.Reportes, "Id", "Nombre", permisosDash.Reportes);
            ViewBag.Reportes = new SelectList(db.Reportes, "Id", "Nombre", permisosDash.Reportes);
            return View(permisosDash);
        }

        // POST: PermisosDash/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Campana,Dashboard,Reportes,Permisos,PermisoTipo,PermisoCampana,PermisoDashboard,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] PermisosDash permisosDash)
        {
            if (ModelState.IsValid)
            {
                db.Entry(permisosDash).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Campana = new SelectList(db.campanas, "Id", "Nombre", permisosDash.Campana);
            ViewBag.Dashboard = new SelectList(db.Dashboard, "Id", "Nombre", permisosDash.Dashboard);
            //ViewBag.PermisoCampana = new SelectList(db.campanas, "Id", "Nombre", permisosDash.PermisoCampana);
            //ViewBag.PermisoDashboard = new SelectList(db.Dashboard, "Id", "Nombre", permisosDash.PermisoDashboard);
            ViewBag.Permisos = new SelectList(db.Reportes, "Id", "Nombre", permisosDash.Reportes);
            ViewBag.Reportes = new SelectList(db.Reportes, "Id", "Nombre", permisosDash.Reportes);
            return View(permisosDash);
        }

        // GET: PermisosDash/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PermisosDash permisosDash = db.permisosdash.Find(id);
            if (permisosDash == null)
            {
                return HttpNotFound();
            }
            return View(permisosDash);
        }

        // POST: PermisosDash/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(Guid id)
        {
            try
            {
                PermisosDash permisosDash = db.permisosdash.Find(id);
                db.permisosdash.Remove(permisosDash);
                db.SaveChanges();
                return Json(new { status = 200 });
            }
            catch (Exception ex)
            {
                return Json(new { status = 500 });
                

            }




        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
