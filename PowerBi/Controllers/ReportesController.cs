﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Models.Security;
using PagedList;
using TarjetasColpatria.Models.Areas;
using TarjetasColpatria.Models.Dashboard;
using TarjetasColpatria.Models.Permisos;
using TarjetasColpatria.Models.Reportes;



namespace TarjetasColpatria.Controllers
{
    public class ReportesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Reportes

        public ActionResult IndexPrincipal(string cuenta, string nombre, int? i)
        {
            TempData["message"] = TempData["message"];

          

            if (!string.IsNullOrEmpty(cuenta) || !string.IsNullOrEmpty(nombre))
            {
                var reportes = db.Reportes.Include(r => r.AreaId)
                                     .Include(r => r.campanaId)
                                     .Include(r => r.Dashboard)
                                     .Include(r => r.tiporeporteId)
                                     .Where(x => x.campanaId.Nombre.Trim().Contains(cuenta.Trim()) || x.Nombre.Trim().Contains(nombre.Trim())).ToList().ToPagedList(i ?? 1, 5);

                return View(reportes);

            }
            else
            {
                var reportes = db.Reportes.Include(r => r.AreaId)
                                      .Include(r => r.campanaId)
                                      .Include(r => r.Dashboard)
                                      .Include(r => r.tiporeporteId)
                                      .ToList().ToPagedList(i ?? 1, 5);

                return View(reportes);

            }


        }

        public ActionResult Index(Guid? Id)
        {
            Dashboard dashboards = db.Dashboard.Find(Id);

            if (dashboards == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Nombre = dashboards.Nombre;
            ViewBag.Id = Id;
            ViewBag.reportes = db.Reportes.Include(s => s.Dashboard).Where(u => u.DashboardId == Id);

            var reportes = db.Reportes;
            return View(reportes.ToList());
        }

        // GET: Reportes/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var reportes = db.Reportes.Find(id);

          

            if (reportes == null)
            {
                return HttpNotFound();
            }
            return View(reportes);
        }
        [CustomAuthorize(Roles = "Administrador")]
        // GET: Reportes/Create
        public ActionResult Create()
        {



            ViewBag.DashboardId = new SelectList(db.Dashboard, "Id", "Nombre");
            ViewBag.area = new SelectList(db.areas, "Id", "Nombre");
            ViewBag.campana = new SelectList(db.campanas.Where(m => m.Areas == db.areas.FirstOrDefault().Id), "Id", "Nombre");   

           ViewBag.DashboardId = new SelectList(db.Dashboard, "Id", "Nombre");
            ViewBag.TIPO_REPORTE = new SelectList(db.tiporeporte, "Id", "Nombre");
            return View();
        }

        public JsonResult Getcampana(Guid Id)
        {

            var camp = db.campanas.Where(m => m.Areas == Id);
            return Json(camp);

        }



        // POST: Reportes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,DashboardId,Estado,url,area,campana,WORKGROUP_NAME,WORKGROUP_ID,REPORT_NAME,REPORT_ID,REPORT_FILTER,TIPO_REPORTE,REPORT_DETALLE,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Reportes reportes, string Nombre)
        {
            if (ModelState.IsValid)
            {

                var arrayCadena = reportes.url.Split('/');
                bool badera = false;

                ViewBag.errorValidacion = "";

                if (reportes.TIPO_REPORTE.ToString().Equals("9009b408-96e8-421a-85ac-03b6d75f9cdf"))//Reporte
                {
                    //Cuando es reporte tiene el siguiente Patron
                    //https://app.powerbi.com/groups/3771fe1b-427c-4163-95af-ab0612ec570c/reports/9b039007-32e0-4eca-b03d-1bb1c9d4d781/ReportSection

                    if (arrayCadena[3].Equals("groups") && arrayCadena[5].Equals("reports"))
                    {
                        badera = true;
                        reportes.WORKGROUP_ID = arrayCadena[4];
                        reportes.REPORT_ID = arrayCadena[6];
                    }
                    else
                    {
                        ViewBag.errorValidacion = "El tipo de reporte no concuerda con la Url idicada.";
                    }

                }
                else
                if (reportes.TIPO_REPORTE.ToString().Equals("e8b65c29-d5eb-4d65-9215-8904b1b9cb90"))//Dashboard
                {



                }


                if (badera == true)
                {
                    reportes.Id = Guid.NewGuid();
                    db.Reportes.Add(reportes);
                    db.SaveChanges();
                    return RedirectToAction("IndexPrincipal");
                }

            }

            var Id = reportes.DashboardId;
            ViewBag.Nombre = Nombre;
            ViewBag.Id = Id;
            ViewBag.reportes = db.Reportes.Include(s => s.Dashboard).Where(u => u.DashboardId == Id);
            ViewBag.area = new SelectList(db.areas, "Id", "Nombre", reportes.area);
            ViewBag.campana = new SelectList(db.campanas, "Id", "Nombre", reportes.campana);
            ViewBag.DashboardId = new SelectList(db.Dashboard, "Id", "Nombre", reportes.DashboardId);
            ViewBag.TIPO_REPORTE = new SelectList(db.tiporeporte, "Id", "Nombre", reportes.TIPO_REPORTE);


            return View();

        }




        // GET: Reportes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Reportes reportes = db.Reportes.Find(id);

            if (reportes == null)
            {
                return HttpNotFound();
            }

            ViewBag.DashboardId = new SelectList(db.Dashboard, "Id", "Nombre", reportes.DashboardId);

            ViewBag.campana = new SelectList(db.campanas, "Id", "Nombre", reportes.campana);

            ViewBag.area = new SelectList(db.areas, "Id", "Nombre", reportes.area);

            ViewBag.TIPO_REPORTE = new SelectList(db.tiporeporte, "Id", "Nombre", reportes.TIPO_REPORTE);

            return View(reportes);
        }

        // POST: Reportes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Reportes reportes)
        {
            if (ModelState.IsValid)
            {

            
                var arrayCadena = reportes.url.Split('/');
                bool badera = false;

                ViewBag.errorValidacion = "";

                if (reportes.TIPO_REPORTE.ToString().Equals("9009b408-96e8-421a-85ac-03b6d75f9cdf"))//Reporte
                {
                //Cuando es reporte tiene el siguiente Patron
                //https://app.powerbi.com/groups/3771fe1b-427c-4163-95af-ab0612ec570c/reports/9b039007-32e0-4eca-b03d-1bb1c9d4d781/ReportSection

                 if(arrayCadena[3].Equals("groups") && arrayCadena[5].Equals("reports"))
                    { 
                     badera = true;
                     reportes.WORKGROUP_ID = arrayCadena[4];
                     reportes.REPORT_ID = arrayCadena[6];
                    }
                    else
                    {
                        ViewBag.errorValidacion = "El tipo de reporte no concuerda con la Url idicada.";
                    }

                }
                else
                if (reportes.TIPO_REPORTE.ToString().Equals("e8b65c29-d5eb-4d65-9215-8904b1b9cb90"))//Dashboard
                {

                    //Cuando es reporte tiene el siguiente Patron
                    //https://app.powerbi.com/groups/d327716e-30bf-456f-8d54-fd006a990d34/dashboards/b4e3cb55-097a-417d-925d-0181c530b64d

                    if (arrayCadena[3].Equals("groups") && arrayCadena[5].Equals("dashboards"))
                    {
                        badera = true;
                        reportes.WORKGROUP_ID = arrayCadena[4];
                        reportes.REPORT_ID = arrayCadena[6];
                    }
                    else
                    {
                        ViewBag.errorValidacion = "El tipo de dashboards no concuerda con la Url idicada.";
                    }

                }

                if(badera == true)
                { 
                    db.Entry(reportes).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index/"+reportes.DashboardId);
                }

            }

            ViewBag.DashboardId = new SelectList(db.Dashboard, "Id", "Nombre", reportes.DashboardId);

            //ViewBag.DashboardId = new SelectList(db.Dashboard, "Id", "Nombre", reportes.DashboardId);

            ViewBag.campana = new SelectList(db.campanas, "Id", "Nombre", reportes.campana);

            ViewBag.area = new SelectList(db.areas, "Id", "Nombre", reportes.area);

            ViewBag.TIPO_REPORTE = new SelectList(db.tiporeporte, "Id", "Nombre", reportes.TIPO_REPORTE);


            return View(reportes);
        }

     
        // POST: Reportes/Delete/5
        [ActionName("Delete")]        
        public ActionResult Delete(Guid id)
        {
            Reportes reportes = db.Reportes.Find(id);
            // Verificamos si este reporte está asociado a un Usuario.
            List<PermisosDash> permisosDash = db.permisosdash.Where(x => x.Reportes == reportes.Id).ToList();
            if (permisosDash.Count > 0)
            {
                TempData["message"] = "No es posible eliminar el reporte, porque depende de otros Usuarios.";
                return RedirectToAction("indexPrincipal");
            }

            db.Reportes.Remove(reportes);
            db.SaveChanges();
            return RedirectToAction("indexPrincipal");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
