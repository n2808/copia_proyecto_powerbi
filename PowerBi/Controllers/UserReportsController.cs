﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Models.Security;
using TarjetasColpatria.Models.Reportes;

namespace TarjetasColpatria.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class UserReportsController : Controller
    {
        private Contexto db = new Contexto();

        // GET: UserReports
        public ActionResult Index()
        {
            var userReports = db.UserReports.Include(u => u.reportes).Include(u => u.user);
            return View(userReports.ToList());
        }

        // GET: UserReports/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserReports userReports = db.UserReports.Find(id);
            if (userReports == null)
            {
                return HttpNotFound();
            }
            return View(userReports);
        }

        // GET: UserReports/Create
        public ActionResult Create()
        {
            ViewBag.IdReporte = new SelectList(db.Reportes, "Id", "Nombre");
            ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Document");
            return View();
        }

        // POST: UserReports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdUsuario,IdReporte,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] UserReports userReports)
        {
            if (ModelState.IsValid)
            {
                userReports.Id = Guid.NewGuid();
                db.UserReports.Add(userReports);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdReporte = new SelectList(db.Reportes, "Id", "Nombre", userReports.IdReporte);
            ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Document", userReports.IdUsuario);
            return View(userReports);
        }

        // GET: UserReports/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserReports userReports = db.UserReports.Find(id);
            if (userReports == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdReporte = new SelectList(db.Reportes, "Id", "Nombre", userReports.IdReporte);
            ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Document", userReports.IdUsuario);
            return View(userReports);
        }

        // POST: UserReports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdUsuario,IdReporte,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] UserReports userReports)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userReports).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdReporte = new SelectList(db.Reportes, "Id", "Nombre", userReports.IdReporte);
            ViewBag.IdUsuario = new SelectList(db.Usuario, "Id", "Document", userReports.IdUsuario);
            return View(userReports);
        }

        // GET: UserReports/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserReports userReports = db.UserReports.Find(id);
            if (userReports == null)
            {
                return HttpNotFound();
            }
            return View(userReports);
        }

        // POST: UserReports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            UserReports userReports = db.UserReports.Find(id);
            db.UserReports.Remove(userReports);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
