﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Core.Models.Security;
using Core.Models;
using TarjetasColpatria.Models.Reportes;
using System.Threading.Tasks;
using PowerBIEmbedded_AppOwnsData.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Rest;
using Microsoft.PowerBI.Api.V2;
using Microsoft.PowerBI.Api.V2.Models;
using Grpc.Core;
using System.IO;

namespace PowerBi.Controllers
{
    public class AccountsController : Controller
    {

        private static readonly string Username = ConfigurationManager.AppSettings["pbiUsername"];
        private static readonly string Password = ConfigurationManager.AppSettings["pbiPassword"];
        private static readonly string AuthorityUrl = "https://login.windows.net/common/oauth2/authorize/";//ConfigurationManager.AppSettings["authorityUrl"];
        private static readonly string ResourceUrl = "https://analysis.windows.net/powerbi/api";// ConfigurationManager.AppSettings["resourceUrl"];
        private static readonly string ClientId = ConfigurationManager.AppSettings["clientId"];
        private static readonly string ApiUrl = "https://api.powerbi.com/";// ConfigurationManager.AppSettings["apiUrl"];
        private static string GroupId = ConfigurationManager.AppSettings["groupId"];
        private static string ReportId = ConfigurationManager.AppSettings["reportId"];


        private Contexto db = new Contexto();

        // GET: Accounts
        public ActionResult Index()
        {
            if (SessionPersister.Id != null)
                return RedirectToAction("Home");

            return View();
        }

        // GET: Accounts

        [CustomAuthorize]
        public ActionResult Home(Guid? Id = null)
        {
            if (Id != null)
            {

                Reportes Reporte = db.Reportes.Find(Id);
                ViewBag.Reporte = Reporte;


                if (Reporte != null)
                    if (Reporte.TIPO_REPORTE.ToString().Equals("9009b408-96e8-421a-85ac-03b6d75f9cdf"))//Reporte
                    {
                        return RedirectToAction("EmbedReport/" + Id);
                    }
                    else
                    {
                        return RedirectToAction("EmbedDashboard/" + Id);
                    }

            }

            return View();

        }

        
        [CustomAuthorize]
        public async Task<ActionResult> EmbedReport(string username, string roles, Guid id)
        {
            #region CODIGO_ANTERIOR
            //Reportes reportes = db.Reportes.Find(id);

            //if (reportes == null)
            //{
            //    return HttpNotFound();
            //}


            ////Con la identificacion del reporte verifico a que campaña a la que pertenece y busco dentro de la tabla PermisosDashes si en verda tieneel permiso asociado

            //ViewBag.NombreReporte = reportes.Nombre.ToUpper();
            //string nombreLogo= db.Logos.Where(x => x.Estado && x.ReporteId == reportes.Id).Select(x => x.Ruta).FirstOrDefault();
            //string ruta = "/Resources/Logos/" + nombreLogo;
            //ViewBag.RutaReporte = ruta;



            //GroupId = reportes.WORKGROUP_ID;
            //ReportId = reportes.REPORT_ID;
            //ViewBag.NombreReporte = reportes.Nombre;

            //var result = new EmbedConfig();
            //try
            //{
            //    result = new EmbedConfig { Username = username, Roles = roles };
            //    var error = GetWebConfigErrors();
            //    if (error != null)
            //    {
            //        result.ErrorMessage = error;
            //        return View(result);
            //    }

            //    // Create a user password cradentials.
            //    var credential = new UserPasswordCredential(Username, Password);

            //    // Authenticate using created credentials
            //    var authenticationContext = new AuthenticationContext(AuthorityUrl);

            //    var authenticationResult = await authenticationContext.AcquireTokenAsync(ResourceUrl, ClientId, credential);

            //    if (authenticationResult == null)
            //    {
            //        result.ErrorMessage = "Authentication Failed.";
            //        return View(result);
            //    }

            //    var tokenCredentials = new TokenCredentials(authenticationResult.AccessToken, "Bearer");

            //    // Create a Power BI Client object. It will be used to call Power BI APIs.
            //    using (var client = new PowerBIClient(new Uri(ApiUrl), tokenCredentials))
            //    {
            //        // Get a list of reports.
            //        var reports = await client.Reports.GetReportsInGroupAsync(GroupId);

            //        Report report;
            //        if (string.IsNullOrEmpty(ReportId))
            //        {
            //            // Get the first report in the group.
            //            report = reports.Value.FirstOrDefault();
            //        }
            //        else
            //        {
            //            report = reports.Value.FirstOrDefault(r => r.Id == ReportId);
            //        }

            //        if (report == null)
            //        {
            //            result.ErrorMessage = "Group has no reports.";
            //            return View(result);
            //        }

            //        var datasets = await client.Datasets.GetDatasetByIdInGroupAsync(GroupId, report.DatasetId);
            //        result.IsEffectiveIdentityRequired = datasets.IsEffectiveIdentityRequired;
            //        result.IsEffectiveIdentityRolesRequired = datasets.IsEffectiveIdentityRolesRequired;
            //        GenerateTokenRequest generateTokenRequestParameters;
            //        // This is how you create embed token with effective identities
            //        if (!string.IsNullOrEmpty(username))
            //        {
            //            var rls = new EffectiveIdentity(username, new List<string> { report.DatasetId });
            //            if (!string.IsNullOrWhiteSpace(roles))
            //            {
            //                var rolesList = new List<string>();
            //                rolesList.AddRange(roles.Split(','));
            //                rls.Roles = rolesList;
            //            }
            //            // Generate Embed Token with effective identities.
            //            generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view", identities: new List<EffectiveIdentity> { rls });
            //        }
            //        else
            //        {
            //            // Generate Embed Token for reports without effective identities.
            //            generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");
            //        }

            //        var tokenResponse = await client.Reports.GenerateTokenInGroupAsync(GroupId, report.Id, generateTokenRequestParameters);

            //        if (tokenResponse == null)
            //        {
            //            result.ErrorMessage = "Failed to generate embed token.";
            //            return View(result);
            //        }

            //        // Generate Embed Configuration.
            //        result.EmbedToken = tokenResponse;
            //        result.EmbedUrl = report.EmbedUrl;
            //        result.Id = report.Id;


            //        //prueba
            //        var ejemplo = report.Id;
            //        ViewBag.ReporteId = report.Id;

            //        return View(result);
            //    }
            //}
            //catch (HttpOperationException exc)
            //{
            //    result.ErrorMessage = string.Format("Status: {0} ({1})\r\nResponse: {2}\r\nRequestId: {3}", exc.Response.StatusCode, (int)exc.Response.StatusCode, exc.Response.Content, exc.Response.Headers["RequestId"].FirstOrDefault());
            //}
            //catch (Exception exc)
            //{
            //    result.ErrorMessage = exc.ToString();
            //}

            #endregion

            ViewBag.reporteId = id;
            ViewBag.url = ConfigurationManager.AppSettings["Url"];

            return View();
        }

        public async Task<ActionResult> EmbedDashboard(Guid id)
        {

            Reportes reportes = db.Reportes.Find(id);

            GroupId = reportes.WORKGROUP_ID;
            ReportId = reportes.REPORT_ID;
            ViewBag.NombreReporte = reportes.Nombre;


            var error = GetWebConfigErrors();
            if (error != null)
            {
                return View(new EmbedConfig()
                {
                    ErrorMessage = error
                });
            }

            // Create a user password cradentials.
            var credential = new UserPasswordCredential(Username, Password);

            // Authenticate using created credentials
            var authenticationContext = new AuthenticationContext(AuthorityUrl);
            var authenticationResult = await authenticationContext.AcquireTokenAsync(ResourceUrl, ClientId, credential);

            if (authenticationResult == null)
            {
                return View(new EmbedConfig()
                {
                    ErrorMessage = "Authentication Failed."
                });
            }

            var tokenCredentials = new TokenCredentials(authenticationResult.AccessToken, "Bearer");

            // Create a Power BI Client object. It will be used to call Power BI APIs.
            using (var client = new PowerBIClient(new Uri(ApiUrl), tokenCredentials))
            {
                // Get a list of dashboards.
                var dashboards = await client.Dashboards.GetDashboardsInGroupAsync(GroupId);

                // Get the first report in the group.

                var dashboard = dashboards.Value.FirstOrDefault();

                if (dashboard == null)
                {
                    return View(new EmbedConfig()
                    {
                        ErrorMessage = "Group has no dashboards."
                    });
                }

                // Generate Embed Token.
                var generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");
                var tokenResponse = await client.Dashboards.GenerateTokenInGroupAsync(GroupId, ReportId, generateTokenRequestParameters);
                //var tokenResponse = await client.Dashboards.GenerateTokenInGroupAsync(GroupId, dashboard.Id, generateTokenRequestParameters);

                if (tokenResponse == null)
                {
                    return View(new EmbedConfig()
                    {
                        ErrorMessage = "Failed to generate embed token."
                    });
                }

                // Generate Embed Configuration.
                var embedConfig = new EmbedConfig()
                {
                    EmbedToken = tokenResponse,
                    EmbedUrl = dashboard.EmbedUrl,
                    Id = ReportId
                    //Id = dashboard.Id
                };

                return View(embedConfig);
            }
        }
        /// <summary>
        /// Check if web.config embed parameters have valid values.
        /// </summary>
        /// <returns>Null if web.config parameters are valid, otherwise returns specific error string.</returns>
        private string GetWebConfigErrors()
        {
            // Client Id must have a value.
            if (string.IsNullOrEmpty(ClientId))
            {
                return "ClientId is empty. please register your application as Native app in https://dev.powerbi.com/apps and fill client Id in web.config.";
            }

            // Client Id must be a Guid object.
            Guid result;
            if (!Guid.TryParse(ClientId, out result))
            {
                return "ClientId must be a Guid object. please register your application as Native app in https://dev.powerbi.com/apps and fill client Id in web.config.";
            }

            // Group Id must have a value.
            if (string.IsNullOrEmpty(GroupId))
            {
                return "GroupId is empty. Please select a group you own and fill its Id in web.config";
            }

            // Group Id must be a Guid object.
            if (!Guid.TryParse(GroupId, out result))
            {
                return "GroupId must be a Guid object. Please select a group you own and fill its Id in web.config";
            }

            // Username must have a value.
            if (string.IsNullOrEmpty(Username))
            {
                return "Username is empty. Please fill Power BI username in web.config";
            }

            // Password must have a value.
            if (string.IsNullOrEmpty(Password))
            {
                return "Password is empty. Please fill password of Power BI username in web.config";
            }

            return null;
        }

        [CustomAuthorize]
        public ActionResult Login()
        {
            //inicio al dashboard
            if (SessionPersister.HasRol("Administrador"))
            {

                return View("View");
            }



            return RedirectToAction("index");
        }



        [HttpPost]

        public ActionResult Login(AccountsViewModel avm)
        {

            AccountsModel am = new AccountsModel();
            Accounts ResultUser = am.Login(avm.Accounts.UserName, avm.Accounts.Password);
            if (string.IsNullOrEmpty(avm.Accounts.UserName) || string.IsNullOrEmpty(avm.Accounts.Password) || ResultUser == null)
            {
                ViewBag.Error = "Usuario o contraseña incorrectos";
                return View("index");
            }


            // get user data for the session.
            SessionPersister.UserName = avm.Accounts.UserName;
            SessionPersister.Id = ResultUser.Id;



            //inicio al dashboard
            if (SessionPersister.HasRol("Administrador"))
            {

                return View("View");
            }



            return RedirectToAction("index");


        }

        public ActionResult Logout()
        {
            SessionPersister.UserName = string.Empty;
            SessionPersister.Id = null;
            return View("index");
        }


        public ActionResult AccessDenied()
        {
            return View();
        }
    }
}