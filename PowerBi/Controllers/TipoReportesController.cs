﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Models.Security;
using TarjetasColpatria.Models.Reportes;

namespace TarjetasColpatria.Controllers
{
    public class TipoReportesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: TipoReportes
        [CustomAuthorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            return View(db.tiporeporte.ToList());
        }

        // GET: TipoReportes/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoReporte tipoReporte = db.tiporeporte.Find(id);
            if (tipoReporte == null)
            {
                return HttpNotFound();
            }
            return View(tipoReporte);
        }

        // GET: TipoReportes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoReportes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Estado,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] TipoReporte tipoReporte)
        {
            if (ModelState.IsValid)
            {
                tipoReporte.Id = Guid.NewGuid();
                db.tiporeporte.Add(tipoReporte);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoReporte);
        }

        // GET: TipoReportes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoReporte tipoReporte = db.tiporeporte.Find(id);
            if (tipoReporte == null)
            {
                return HttpNotFound();
            }
            return View(tipoReporte);
        }

        // POST: TipoReportes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Estado,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] TipoReporte tipoReporte)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoReporte).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoReporte);
        }

        // GET: TipoReportes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoReporte tipoReporte = db.tiporeporte.Find(id);
            if (tipoReporte == null)
            {
                return HttpNotFound();
            }
            return View(tipoReporte);
        }

        // POST: TipoReportes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            TipoReporte tipoReporte = db.tiporeporte.Find(id);
            db.tiporeporte.Remove(tipoReporte);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
