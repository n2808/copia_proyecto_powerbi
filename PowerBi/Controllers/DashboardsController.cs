﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Models.Security;
using TarjetasColpatria.Models;
using TarjetasColpatria.Models.Dashboard;

namespace TarjetasColpatria.Controllers
{
    
    public class DashboardsController : Controller
    {
        private Contexto db = new Contexto();


        [CustomAuthorize(Roles = "Administrador,Reporting,Gerente,Soporte")]
        public ActionResult IndexPrincipal()
        {
            var dashboard = db.Dashboard.Include(c => c.campana);
            return View(dashboard.ToList());
        }

       
        // GET: Dashboards
        public ActionResult Index(Guid? Id)
        {
           
             campanas campana = db.campanas.Find(Id);
            if (campana == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Nombre = campana.Nombre;
            ViewBag.Id = Id;
            ViewBag.dahsboard = db.Dashboard.Include(s => s.campana).Where(u => u.CampanaId == Id);
            return View();

        }

        // GET: Dashboards/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           
            Dashboard dashboard = db.Dashboard.Include(x => x.campana).Where(x => x.Id == id).FirstOrDefault();
            if (dashboard == null)
            {
                return HttpNotFound();
            }
            return View(dashboard);
        }
        [CustomAuthorize(Roles = "Administrador")]

        // GET: Dashboards/Create
        public ActionResult Create()
        {
          
            ViewBag.CampanaId = new SelectList(db.campanas, "Id", "Nombre");
            return View();
        }

        // POST: Dashboards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Estado,CampanaId,DashboardId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Dashboard dashboard )
        {
            if (ModelState.IsValid)
            {
                dashboard.Id = Guid.NewGuid();
                db.Dashboard.Add(dashboard);
                db.SaveChanges();
                return RedirectToAction("IndexPrincipal");
            }

            var Id = dashboard.CampanaId;
            ViewBag.Nombre = dashboard.Nombre;
            ViewBag.Id = Id;
            ViewBag.dahsboard = db.Dashboard.Include(s => s.campana).Where(u => u.CampanaId == Id);
            
            return View("IndexPrincipal");
        }

        // GET: Dashboards/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dashboard dashboard = db.Dashboard.Find(id);
            if (dashboard == null)
            {
                return HttpNotFound();
            }
            ViewBag.CampanaId = new SelectList(db.campanas, "Id", "Nombre", dashboard.CampanaId);
            return View(dashboard);
        }

        // POST: Dashboards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Estado,CampanaId,DashboardId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Dashboard dashboard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dashboard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index/"+ dashboard.CampanaId);
            }
            ViewBag.CampanaId = new SelectList(db.campanas, "Id", "Nombre", dashboard.CampanaId);
            return View(dashboard);
        }

        // GET: Dashboards/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dashboard dashboard = db.Dashboard.Find(id);
            if (dashboard == null)
            {
                return HttpNotFound();
            }
            return View(dashboard);
        }


        // POST: Dashboards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Dashboard dashboard = db.Dashboard.Find(id);
            db.Dashboard.Remove(dashboard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
