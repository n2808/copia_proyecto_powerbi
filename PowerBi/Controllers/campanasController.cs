﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Models.Security;
using TarjetasColpatria.Models;

namespace TarjetasColpatria.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class campanasController : Controller
    {
        private Contexto db = new Contexto();

        // GET: campanas
         public ActionResult Index()
        {
            var campanas = db.campanas.Include(c => c.AreasId);
            return View(campanas.ToList());
        }

        // GET: campanas/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            campanas campanas =db.campanas.Include(J => J.AreasId).Where(c => c.Id == id).FirstOrDefault();
            //campanas campanas = db.campanas.Find(id);

            if (campanas == null)
            {
                return HttpNotFound();
            }
            return View(campanas);
        }

        // GET: campanas/Create
        public ActionResult Create()
        {
            ViewBag.Areas = new SelectList(db.areas, "Id", "Nombre");
            ViewBag.dashboard = new SelectList(db.Dashboard, "Id", "Nombre");
            return View();
        }

        // POST: campanas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Estado,Areas,dashboard,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] campanas campanas)
        {
            if (ModelState.IsValid)
            {
                campanas.Id = Guid.NewGuid();
                db.campanas.Add(campanas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Areas = new SelectList(db.areas, "Id", "Nombre", campanas.Areas);
            return View(campanas);
        }

        // GET: campanas/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            campanas campanas = db.campanas.Find(id);
            if (campanas == null)
            {
                return HttpNotFound();
            }

            ViewBag.Areas = new SelectList(db.areas, "Id", "Nombre", campanas.Areas);
            return View(campanas);
        }

        // POST: campanas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Estado,Areas,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] campanas campanas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(campanas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Areas = new SelectList(db.areas, "Id", "Nombre", campanas.Areas);
            return View(campanas);
        }

       
        // GET: campanas/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            campanas campanas = db.campanas.Find(id);
            if (campanas == null)
            {
                return HttpNotFound();
            }
            return View(campanas);
        }

        // POST: campanas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            campanas campanas = db.campanas.Find(id);
            db.campanas.Remove(campanas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
