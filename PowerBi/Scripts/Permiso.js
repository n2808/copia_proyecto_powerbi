﻿
    $(document).ready(function () {
        $("#Campana").change(function () {
            $("#Dashboard").empty();
            $("#Reportes").empty();
            if ($("#Campana").val() == "") {
                $("#Dashboard").val("").select2();
                $("#Reportes").val("").select2();
                return;
            }
            $.ajax({
                type: 'POST',
                url: '/Users/GetDashboards',
                dataType: 'json',
                data: { Id: $("#Campana").val() },
                success: function (dash) {
                    $("#Dashboard").append('<option value="-1" selected>Todas</option>').select2();
                    $("#Dashboard").select2();
                    $.each(dash, function (i, dash) {
                        $("#Dashboard").append('<option value="'
                            + dash.Id + '">'
                            + dash.Nombre + '</option>');

                    });
                },


                error: function (ex) {
                    alert('error al visualizar los dashboards' + ex);
                }
            });
            return false;
        })
    });

        $(document).ready(function () {
        $("#Dashboard").change(function () {
            $("#Reportes").empty();
            var IdDash = $("#Dashboard").val();
            $("#Reportes").append('<option value="-1" selected>Todos</option>');
            $("#Reportes").select2();

            if (IdDash == -1) {
                $("#Reportes").val("-1").select2();
                return;
            }
            if (IdDash == null) {
                $("#Reportes").val("").select2();
                return;
            }


            $.ajax({
                type: 'POST',
                url: '/Users/GetReportes',
                dataType: 'json',
                data: { Id: IdDash },
                success: function (Reports) {
                
                    $.each(Reports, function (i, Reports) {
                        $("#Reportes").append('<option value="'
                            + Reports.Id + '">'
                            + Reports.Nombre + '</option>');
                    });
                },
                error: function (ex) {
                    alert('error al visualizar los reportes' + ex);
                }
            });
            return false;
        })
});



