﻿var _txtDireccion = null;
// validarCuadranteViaPrincipal();validarCuadranteViaGeneradora();editarDireccionNotificacion2();
// Asigna los valores del registro a editar 
function limpiarValoresEditarDireccion() {
    $('#direccionEditar').val("");
    setPorNombre('tipoViaPrimaria', '');
    setPorNombre('numeroViaPrincipal', '');
    setPorNombre('letraViaPrincipal', '');
    setPorNombre('bis', '');
    setPorNombre('letraBis', '');
    setPorNombre('cuadranteViaPrincipal', '');
    setPorNombre('numeroViaGeneradora', '');
    setPorNombre('letraViaGeneradora', '');
    setPorNombre('cuadranteViaGeneradora', '');

}

/* Asigna los valores del registro a editar */
function asignarValoresEditarDireccion() {
    if (leerTexto('#direccionEditar') != leerTexto(_txtDireccion)) {
        limpiarValoresEditarDireccion();
    };
}

//Muestra el panel para editar un registro o crear un nuevo
function mostrarPanelEditarDireccion() {
    var titulo;
    titulo = "Modificar Dirección";
    asignarValoresEditarDireccion();

    mostrarPopup('#frmDireccionNotificacion', titulo, "auto", "auto");
    return false;
}

//Porcesa el evento nuevo
function modificarDireccion() {
    //Detectar el texto a modificar
    var boton = $(this);
    _txtDireccion = boton.prev();
    mostrarPanelEditarDireccion();
    return false;
}

function editarDireccionNotificacion2() {
    this.document.frmDireccionNotificacion.direccion.value =
        this.document.frmDireccionNotificacion.tipoViaPrimaria.options[this.document.frmDireccionNotificacion.tipoViaPrimaria.selectedIndex].value;
    if (this.document.frmDireccionNotificacion.tipoViaPrimaria.selectedIndex >= 0) {
        if (this.document.frmDireccionNotificacion.tipoViaPrimaria.options[this.document.frmDireccionNotificacion.tipoViaPrimaria.selectedIndex].value != "AV") {

            if (this.document.frmDireccionNotificacion.numeroViaPrincipal.value != "") {
                this.document.frmDireccionNotificacion.direccion.value += " " + this.document.frmDireccionNotificacion.numeroViaPrincipal.value;
            }
            if (this.document.frmDireccionNotificacion.letraViaPrincipal.selectedIndex >= 0) {

                if (this.document.frmDireccionNotificacion.letraViaPrincipal.options[this.document.frmDireccionNotificacion.letraViaPrincipal.selectedIndex].value != "") {
                    this.document.frmDireccionNotificacion.direccion.value += this.document.frmDireccionNotificacion.letraViaPrincipal.options[this.document.frmDireccionNotificacion.letraViaPrincipal.selectedIndex].value;
                }

            }


            if (this.document.frmDireccionNotificacion.bis.selectedIndex >= 0) {

                if (this.document.frmDireccionNotificacion.bis.options[this.document.frmDireccionNotificacion.bis.selectedIndex].value != "") {
                    this.document.frmDireccionNotificacion.direccion.value += " " + this.document.frmDireccionNotificacion.bis.options[this.document.frmDireccionNotificacion.bis.selectedIndex].value;
                }
            }

            if (this.document.frmDireccionNotificacion.letraBis.selectedIndex >= 0) {

                if (this.document.frmDireccionNotificacion.letraBis.options[this.document.frmDireccionNotificacion.letraBis.selectedIndex].value != "") {
                    this.document.frmDireccionNotificacion.direccion.value += " " + this.document.frmDireccionNotificacion.letraBis.options[this.document.frmDireccionNotificacion.letraBis.selectedIndex].value;
                }

            }


            if (this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex >= 0) {
                if (this.document.frmDireccionNotificacion.cuadranteViaPrincipal.options[this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex].value != "") {
                    this.document.frmDireccionNotificacion.direccion.value += " " + this.document.frmDireccionNotificacion.cuadranteViaPrincipal.options[this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex].value;
                }
            }

        }
        else {
            //  this.document.frmDireccionNotificacion.direccion.value=  this.document.frmDireccionNotificacion.tipoViaPrimaria.options[ this.document.frmDireccionNotificacion.tipoViaPrimaria.selectedIndex].value
            //  this.document.frmDireccionNotificacion.direccion.value+=" "+ this.document.frmDireccionNotificacion.nombreViaPrincipal.value ;
        }

        if (this.document.frmDireccionNotificacion.numeroViaGeneradora.value != "") {
            this.document.frmDireccionNotificacion.direccion.value += " " + this.document.frmDireccionNotificacion.numeroViaGeneradora.value;
        }

        if (this.document.frmDireccionNotificacion.letraViaGeneradora.selectedIndex >= 0) {

            if (this.document.frmDireccionNotificacion.letraViaGeneradora.options[this.document.frmDireccionNotificacion.letraViaGeneradora.selectedIndex].value != "") {
                this.document.frmDireccionNotificacion.direccion.value += this.document.frmDireccionNotificacion.letraViaGeneradora.options[this.document.frmDireccionNotificacion.letraViaGeneradora.selectedIndex].value;
            }

        }

        if (this.document.frmDireccionNotificacion.numeroPlaca.value != "") {
            var cero = "";
            if (this.document.frmDireccionNotificacion.numeroPlaca.value < 10) {
                cero = "0";
            }
            this.document.frmDireccionNotificacion.direccion.value += " " + cero + this.document.frmDireccionNotificacion.numeroPlaca.value;
        }
        if (this.document.frmDireccionNotificacion.cuadranteViaGeneradora.selectedIndex >= 0) {
            this.document.frmDireccionNotificacion.direccion.value += " " + this.document.frmDireccionNotificacion.cuadranteViaGeneradora.options[this.document.frmDireccionNotificacion.cuadranteViaGeneradora.selectedIndex].value;
        }
    }




}


function habilitarNombreAvenida() {
    if (this.document.frmDireccionNotificacion.tipoViaPrimaria.selectedIndex >= 0) {
        if (this.document.frmDireccionNotificacion.tipoViaPrimaria.options[this.document.frmDireccionNotificacion.tipoViaPrimaria.selectedIndex].value == "AV") {
            this.document.frmDireccionNotificacion.numeroViaPrincipal.disabled = true;
            this.document.frmDireccionNotificacion.nombreViaPrincipal.disabled = false;

        }
        else {
            this.document.frmDireccionNotificacion.numeroViaPrincipal.disabled = false;
            this.document.frmDireccionNotificacion.nombreViaPrincipal.disabled = true;
        }
    }
}
function validarCuadranteViaPrincipal() {
    if (this.document.frmDireccionNotificacion.tipoViaPrimaria.selectedIndex >= 0) {
        var tipoViaPrim = this.document.frmDireccionNotificacion.tipoViaPrimaria.options[this.document.frmDireccionNotificacion.tipoViaPrimaria.selectedIndex].value;
        if (this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex >= 0) {
            var cuadranteViaPrim = this.document.frmDireccionNotificacion.cuadranteViaPrincipal.options[this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex].value;

            if (tipoViaPrim == "CL" || tipoViaPrim == "AC" || tipoViaPrim == "DG") {
                if (cuadranteViaPrim == "ESTE") {
                    console.log("este1")
                    this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex = "-1";
                }
            }
            if (tipoViaPrim == "KR" || tipoViaPrim == "AK" || tipoViaPrim == "TV") {
                if (cuadranteViaPrim == "SUR") {
                    this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex = "-1";
                }
            }
        }
        if (tipoViaPrim == "AV") {
            this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex = "-1";
        }

    }

}



function editarComplemento() {
    var vacio = "";
    if (this.document.frmDireccionNotificacion.componenteComplemento.selectedIndex >= 0 && this.document.frmDireccionNotificacion.valorComplemento.value != "") {
        if (this.document.frmDireccionNotificacion.direccion.value != "") {
            vacio = " ";
        }
        this.document.frmDireccionNotificacion.direccion.value += vacio + this.document.frmDireccionNotificacion.componenteComplemento.options[this.document.frmDireccionNotificacion.componenteComplemento.selectedIndex].value;
        this.document.frmDireccionNotificacion.direccion.value += " " + this.document.frmDireccionNotificacion.valorComplemento.value;
    }


}


function validarCuadranteViaGeneradora() {

    if (this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex >= 0) {
        var cuadranteViaPrim = this.document.frmDireccionNotificacion.cuadranteViaPrincipal.options[this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex].value;
        if (this.document.frmDireccionNotificacion.cuadranteViaGeneradora.selectedIndex >= 0) {
            var cuadranteViaGeneradora = this.document.frmDireccionNotificacion.cuadranteViaGeneradora.options[this.document.frmDireccionNotificacion.cuadranteViaGeneradora.selectedIndex].value;

            if (cuadranteViaPrim == cuadranteViaGeneradora) {
                this.document.frmDireccionNotificacion.cuadranteViaGeneradora.selectedIndex = "-1";
                this.document.frmDireccionNotificacion.cuadranteViaPrincipal.selectedIndex = "-1";
            }
        }

    }


}


function borrarDireccion() {
    this.document.frmDireccionNotificacion.direccion.value = "";
}

function borrarComplementoEscogido() {
    this.document.frmDireccionNotificacion.componenteComplemento.selectedIndex = 0;
    this.document.frmDireccionNotificacion.valorComplemento.value = "";
}

//Agregar elementos desde lista
function agregarElementosDesdeSelectALista(select, lista) {
    $(select + " option").each(function () {
        var valor = $(this).val();
        if (!isEmpty(valor)) {
            lista.push(valor);
        }
    });
}
//Indica si alguno de los elementos de la lista
function getContainsAny(texto, lista) {
    var contains = false;
    $.each(lista, function () {
        if (texto.indexOf(this) >= 0) {
            contains = true;
        }
    });
    return contains;
}
//Verifica los datos
function verificarDatosDireccion(direccion) {
    var mensaje = "";
    if (!isEmpty(direccion)) {
        var lista = [];
        agregarElementosDesdeSelectALista("#listaComplemento", lista);
        if (direccion.indexOf("AC") == -1 && direccion.indexOf("AK") == -1
            && direccion.indexOf("CL") == -1 && direccion.indexOf("KR") == -1
            && direccion.indexOf("DG") == -1 && direccion.indexOf("TV") == -1
            && !getContainsAny(direccion, lista)) {
            mensaje += "\n- La primera opción: avenida, calle, cra, diagonal es obligatoria o debe especificar el complemento";
        }
    }

    var ok = isEmpty(mensaje);
    if (!ok) {
        mostrarMensajeVerificarDatos(mensaje);
    }
    return ok;
}

function aceptarDireccion() {
    var direccion = leerTexto('#direccionEditar');
    if (verificarDatosDireccion(direccion)) {
        $(_txtDireccion).val(direccion);
        cerrarPopup('#frmDireccionNotificacion');
        $(_txtDireccion).change();
    }
}


function inicializar() {

    $('#btnAceptarDireccion').click(aceptarDireccion);

}

$(document).ready(inicializar);

var request;
request = 0;
//var contadorEnvio;
contadorEnvio = 0;

function func_enviarFormulario(event, formulario, url, divServerRespuesta) {
    event.preventDefault();
    //alert(request);
    if (!request) {
        request = 1;
        //contadorEnvio=contadorEnvio+1;
        var url = url;
        $("#loader_gif").fadeIn("slow");

        //var datosFormulario = encodeURI($("#"+formulario).serialize());

        request = $.ajax({
            type: "POST",
            url: url,
            data: $("#" + formulario).serialize(),
            success: function (datos) {
                $("#loader_gif").fadeOut("slow");
                $("#" + divServerRespuesta).html(datos);
            },
            error: function () {
                //alert("Se ha producido un error");
                request = 0;
            },
            complete: function () { request = 0; }
        });

    }
    // else
    //  alert('El formulario ya fue enviado. Por favor espere..');
    //request = null; //esto va fuera del ajax pero dentro del click que lo detona 
    return false;

}

function func_enviarFormularioV2(event, formulario, url, divServerRespuesta, idGif) {

    event.preventDefault();

    //alert(request);
    if (!request) {
        request = 1;
        //contadorEnvio=contadorEnvio+1;
        var url = url;
        $("#" + idGif).fadeIn("slow");

        //var datosFormulario = encodeURI($("#"+formulario).serialize());

        request = $.ajax({
            type: "POST",
            url: url,
            data: $("#" + formulario).serialize(),
            success: function (datos) {
                $("#" + idGif).fadeOut("slow");
                $("#" + divServerRespuesta).html(datos);
            },
            error: function () {
                //alert("Se ha producido un error");
                request = 0;
            },
            complete: function () { request = 0; }
        });

    }
    // else
    //  alert('El formulario ya fue enviado. Por favor espere..');
    //request = null; //esto va fuera del ajax pero dentro del click que lo detona 
    return false;

}


//----------------------------------------------
function cargarURL_V2(urlV, titulo, titleButton, formulario, url, divServerRespuesta, idGif) {

    //var parametros =parametrosE.split("|");

    $("#cargarUrl").load(urlV);
    $("#idVentana").html(titulo);

    var eventButton = "func_enviarFormularioV2(event,'" + formulario + "','" + url + "','" + divServerRespuesta + "','" + idGif + "');";


    $("#ponerBoton").html("<button class=\"btn btn-primary\" onclick=\"" + eventButton + "\">" + titleButton + "</button>");

    //options= {keyboard: false};//http://bootstrapdocs.com/v2.2.2/docs/javascript.html#modals
    $('#ModalDireccion').modal();

}
function asignarDireccion() {

    var direccion = document.getElementById("direccionEditar").value;

    if (direccion.length <= 400) {

        document.getElementById("txt_direccion").value = document.getElementById("direccionEditar").value;
        $('#ModalDireccion').modal('hide');
    }
    else
        alert("Atención: La dirección no puede tener mas de 400 Caracteres");

}

function asignarDireccionUbicacion() {

    var direccion = document.getElementById("direccionEditar").value;

    if (direccion.length <= 400) {

        document.getElementById("sol_direccion").value = document.getElementById("direccionEditar").value;
        $('#ModalDireccion').modal('hide');

    }
    else
        alert("Atención: La dirección no puede tener mas de 20 Caracteres");

}
//-------------------------------------------------------------------------------------------------------
function asignarDireccionTrabajo() {

    var direccion = document.getElementById("direccionEditar").value;

    if (direccion.length <= 400) {
        document.getElementById("sol_direc_trabajo").value = document.getElementById("direccionEditar").value;
        $('#ModalDireccion').modal('hide');
    }
    else
        alert("Atención: La dirección no puede tener mas de 20 Caracteres");

}

function cargarURL(urlV, titulo, eventButton, titleButton) {

    $("#cargarUrl").load(urlV);
    $("#idVentana").html(titulo);

    $("#ponerBoton").html("<button class=\"btn btn-primary\" onclick=\"" + eventButton + "();\">" + titleButton + "</button>");

    //options= {keyboard: false};//http://bootstrapdocs.com/v2.2.2/docs/javascript.html#modals
    $('#ModalDireccion').modal();

}
//Va la gestion y se puede agregar mas parametros
//function traerBeneficiarios(parametros, idProd) {

//    //$("#idBeneficiarios").load("controlador/listaBeneficiariosC.php?idGestion="+parametros);
//    //Dentro de parametros debe de enviarse el codigo del producto
//    $("#divBeneProduc_" + idProd).load("controlador/listaBeneficiariosC.php?idGestion=" + parametros);

//}

function productoSeleccionado(opcion) {

    //alert(opcion.value);

    switch (opcion.value) {

        case "1":
            $("#des_numero_cuenta").attr('disabled', 'disabled');
            $("#des_tipoCuenta").attr('disabled', 'disabled');
            $("#sol_entidad_bancaria").attr('disabled', 'disabled');
            $("#sol_plazoMeses").attr('disabled', 'disabled');
            $("#sol_tipoCreditoConsumo").attr('disabled', 'disabled');
            $("#sol_libranzaDescuNomina").attr('disabled', 'disabled');

            //alert(opcion.value);

            break;
        case "2":
            $("#des_numero_cuenta").removeAttr("disabled");
            $("#des_tipoCuenta").removeAttr("disabled");
            $("#sol_entidad_bancaria").removeAttr("disabled");
            $("#sol_plazoMeses").removeAttr("disabled");
            $("#sol_tipoCreditoConsumo").removeAttr("disabled");
            $("#sol_libranzaDescuNomina").removeAttr("disabled");

            break;

    }


}

//Calcula l valor maximo de cuota
function valorMaximoCuota() {
    var maximoCuota = Math.round($("#sol_valorSolicitado").val() * 0.05, 0);
    var solicitado = parseInt($("#sol_valorSolicitado").val());

    if (solicitado <= 800000)
        $("#sol_cuotaMaxima").val(maximoCuota);
    else
        $("#sol_cuotaMaxima").val("");
}

function informacionConyugue(dato) {

    var estadoCivil = dato.value;

    if (estadoCivil == "2" || estadoCivil == "3") {
        $("#id_datosConyugue").show();

        $("#coy_apellido1").removeAttr("disabled");
        $("#coy_apellido2").removeAttr("disabled");
        $("#coy_nombre1").removeAttr("disabled");
        $("#coy_nombre2").removeAttr("disabled");
        $("#loo_coy_tipo_docu").removeAttr("disabled");
        $("#coy_docume_conyugue").removeAttr("disabled");
        $("#coy_cargo_actual").removeAttr("disabled");
        //$("#coy_trabaja").removeAttr("disabled");
        $("#coy_trabaja").val("NO");
        $("#coy_ciudad_empresa").removeAttr("disabled");
        $("#coy_empresa").removeAttr("disabled");
        $("#coy_empresa_direc").removeAttr("disabled");
        $("#coy_tel_empresa").removeAttr("disabled");
        $("#coy_celular").removeAttr("disabled");
        $("#coy_correo").removeAttr("disabled");


    }
    else {

        $("#coy_apellido1").attr('disabled', 'disabled');
        $("#coy_apellido2").attr('disabled', 'disabled');
        $("#coy_nombre1").attr('disabled', 'disabled');
        $("#coy_nombre2").attr('disabled', 'disabled');
        $("#loo_coy_tipo_docu").attr('disabled', 'disabled');
        $("#coy_docume_conyugue").attr('disabled', 'disabled');
        $("#coy_cargo_actual").attr('disabled', 'disabled');
        //$("#coy_trabaja").attr('disabled', 'disabled');
        $("#coy_trabaja").val("");
        $("#coy_ciudad_empresa").attr('disabled', 'disabled');
        $("#coy_empresa").attr('disabled', 'disabled');
        $("#coy_empresa_direc").attr('disabled', 'disabled');
        $("#coy_tel_empresa").attr('disabled', 'disabled');
        $("#coy_celular").attr('disabled', 'disabled');
        $("#coy_correo").attr('disabled', 'disabled');

        $("#id_datosConyugue").hide();

    }

}

function asignarGrupoFamiliar(form) {

    var valor = form.value;
    var valor1 = parseInt(valor) + 1;
    //alert(valor);

    $("#sol_total_gruo_fam").val(valor1);
    $("#divnumGrupoFam").html(valor1);


}

function validarCadena(e, tipo) { // 1
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla == 8) return true; // 3
    if (tecla == 0) return true; // no se que es cero. pero dejo que ingrese. Me permite usar Tab

    patron = /[A-Za-z\d\s\.\,\#\(\)\@\-\;\:\/]/; // 4
    if (tipo == 1) //Numerico
        patron = /[\d]/; // Solo numeros
    else
        if (tipo == 3) //Numerico con punto
            patron = /[0-9\.]/;

    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6+
}

function traerMunicipios(idDept, opcion) {
    $("#divMunicipio").load("controlador/variosC.php?opc=" + opcion + "&idDept=" + idDept.value);
}

function traerMunicipiosDB(idDept, opcion, idMunicipio) {
    $("#divMunicipio").load("controlador/variosC.php?opc=" + opcion + "&idDept=" + idDept + "&idMunicipio=" + idMunicipio);
}
function listaPlanesActivos(idGestion) {
    $("#idlistaPlanes").load("controlador/planesActvosProductoC.php?idGestion=" + idGestion);
}
//-------------------------------------------------------------------------------------------------------------
function adquiereSeguro(datos) {

    //var opc=datos.value;


    if ($("#idinteresadoSeguroSi").is(':checked'))
        $("#IdadquirirSeguro").show();
    else
        $("#IdadquirirSeguro").hide();

}