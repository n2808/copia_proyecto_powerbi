﻿var IdUser = null;
$("body").on('click', ".consultarUsuarioPermisos", function () {

    var s_elm = $(this);
    var id = s_elm.data('id')
    IdUser = id;
    $.post('/Users/' + 'GetPermisos', { Id: id })
        .done(function (Result) {
            var tablaCuerpo = $("#permisos tbody");
            tablaCuerpo.html("");
            $.each(Result, function (index, val) {
                var Campana = val.campanaId != null ? val.campanaId.Nombre : "N/A";
                var Dasboards = val.dashboardId != null ? val.dashboardId.Nombre : "Todos";
                var Reportes = val.reportesId != null ? val.reportesId.Nombre : "Todos";

                var fila = "<tr>\
		                <td>"+ Campana+ "</td>\
                        <td>"+ Dasboards + "</td>\
                        <td>"+ Reportes + "</td>\
                    <td> <div class='btn bg-red btn-xs removerPermiso' data-id='"+ val.Id + "' > Remover</div></</td >\
		                   </tr>";

                tablaCuerpo.append(fila);

            })
            $("#myModal").modal().show();
        })

})


// funcioin para asignar los permisos a la dashboard
$("body").on('click', ".CrearUsuarioPermisos", function () {

    var s_elm = $(this);
    var id = s_elm.data('id')



    // definicion de las variables de entorno.


    var campanaPermiso = $("#Dashboard").val() == "-1" ? "true" : "false";
    var PermisodeDashboard = $("#Reportes").val() == "-1" ? "true" : "false";
    if ($("#Campana").val() == "" || $("#Campana").val() == null) {
        alert("por favor seleccione la campaña")
        return;
    }
    var PermisoDash = {
        Campana: $("#Campana").val(),
        Dashboard: $("#Dashboard").val(),
        Reportes: $("#Reportes").val(),
        PermisosUsers: IdUser,
        PermisodeDashboard: PermisodeDashboard, // true or false,
        campanaPermiso: campanaPermiso, // true or false,
    };

    $.post('/permisosDash/' + 'AsignarPermiso', PermisoDash )
        .done(function (Result) {
            var tablaCuerpo = $("#permisos tbody");
            tablaCuerpo.html("");
            $.each(Result.message, function (index, val) {
                var Campana = val.campanaId != null ? val.campanaId.Nombre : "N/A";
                var Dasboards = val.dashboardId != null ? val.dashboardId.Nombre : "Todos";
                var Reportes = val.reportesId != null ? val.reportesId.Nombre : "Todos";

                var fila = "<tr>\
		                <td>"+ Campana + "</td>\
                        <td>"+ Dasboards + "</td>\
                        <td>"+ Reportes + "</td>\
                    <td> <div class='btn bg-red btn-xs removerPermiso' data-id='"+ val.Id + "' > Remover</div></</td >\
		                   </tr>";

                tablaCuerpo.append(fila);

            })

        })

})


// Quita el permiso que tiene un usuario.
$("body").on('click', ".removerPermiso", function () {

    var s_elm = $(this);
    var id = s_elm.data('id')

    $.post('/permisosDash/' + 'Delete', { Id: id, __RequestVerificationToken : $("input[name=__RequestVerificationToken]").val() } )
        .done(function (Result) {
            s_elm.parents('tr').remove();
        })

})


