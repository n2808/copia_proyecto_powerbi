﻿DEV_MODE = true;
SHOW_ALERT = false;
let consecutivo_tip = 1;


function _console(msg, title) {
    if (DEV_MODE == false) { return };
    SHOW_ALERT ? alert(msg) :
        console.log('%c|...............................................', 'color: #00a7d0') +
        console.log(msg) +
        console.log('%c|...............................................', 'color: #00a7d0');
}
function _error(msg) {
    if (DEV_MODE == false) { return };
    SHOW_ALERT ? alert(msg) :
        console.log('%c|...............................................', 'color: #df604a') +
        console.log(msg) +
        console.log('%c|...............................................', 'color: #df604a');
}

function normalizeTrueFalse(value) {
    return value == true ? 1 : 0;
}

/*-----------------------------------------------------------------------------------*/
/*---------| CLASE DE EVENTOS GENERALES
/*-----------------------------------------------------------------------------------*/


class General {
    constructor() {
        this.url = {};
        this.url.Gestion = BaseUrl + "Gestions/";
        this.url.back = BaseUrl + "GestionActivacion/";
        this.url.AyudaVenta = BaseUrl + "Ayudas/";
        this.data = {};

    }

    toBottom() {
        window.scrollTo(0, document.body.scrollHeight);
    }

    PopulateDropdown(Result, obj) {
        $(obj).append('<option value="0">Sin Seleccionar</option>');
        $.each(Result, function (index, Value) {
            $(obj).append('<option value="'
                + Value.Value + '">' +
                Value.Text + '</option>');
        });
    }

    JsonDecodeErrors(ErrorObject) {
        if (ErrorObject.Type == 0) {
            return string_mensaje(ErrorObject.Message, "alert-danger");
        }
        else
            return string_mensaje(ErrorObject.Message + "[" + ErrorObject.Name + "]", "alert-danger");
    }

}

$(document).ready(function () {
    $('.sidebar-menu').tree()
    $(".datepicker").datepicker();
    $(".select2").select2();
})