﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TarjetasColpatria.Models.Reportes
{
    public class Reportes : Entity
    {
        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Dashboard")]
        public Guid? DashboardId { get; set; }

        [DisplayName("Estado")]
        public Boolean Estado { get; set; }

        [DisplayName("Url")]
        public string url { get; set; }

        public Guid area { get; set; }

        public Guid campana { get; set; }

        public string WORKGROUP_NAME { get; set; }

        public string WORKGROUP_ID { get; set; }

        public string REPORT_NAME { get; set; }

        public string REPORT_ID { get; set; }

        public string REPORT_FILTER { get; set; }

        public Guid TIPO_REPORTE { get; set; }

        public string REPORT_DETALLE { get; set; }

                          
        //llaves foranea


        [ForeignKey("DashboardId")]
        public Dashboard.Dashboard Dashboard { get; set; }

        [ForeignKey("area")]
        public Areas.Areas AreaId { get; set; }

        [ForeignKey("campana")]
        public campanas campanaId { get; set; }

        [ForeignKey("TIPO_REPORTE")]
        public TipoReporte tiporeporteId { get; set; }
    }

 }
