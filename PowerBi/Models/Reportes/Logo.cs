﻿using Core.Models.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TarjetasColpatria.Models.Reportes
{
    public class Logo : EntityWithIntId
    {
        public bool Estado { get; set; } = true;
        public string Ruta { get; set; }
        public Guid? ReporteId { get; set; }

        [ForeignKey("ReporteId")]
        public Reportes Reporte { get; set; }
    }
}