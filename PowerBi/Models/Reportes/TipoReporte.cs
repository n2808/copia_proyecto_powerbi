﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TarjetasColpatria.Models.Reportes
{
    public class TipoReporte:Entity
    {

        public string Nombre { get; set; }

        public Boolean Estado { get; set; }

    }
}