﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TarjetasColpatria.Models.Reportes
{
    public class UserReports:Entity
    {
        [DisplayName("Usuario")]
        public Guid IdUsuario { get; set; }

        [DisplayName("Reporte")]
        public Guid IdReporte { get; set; }



        //foraneas

        [ForeignKey("IdUsuario")]
        public User user { get; set; }

        [ForeignKey("IdReporte")]
        public Reportes reportes { get; set; }


    }
}