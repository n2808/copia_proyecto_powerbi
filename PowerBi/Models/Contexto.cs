﻿namespace Core.Models
{
    using Core.Models.configuration;
    using Core.Models.User;
    using System;
    using System.Data.Entity;
    using System.Linq;
    using TarjetasColpatria.Models;
    using TarjetasColpatria.Models.Areas;
    using TarjetasColpatria.Models.Dashboard;
    using TarjetasColpatria.Models.Permisos;
    using TarjetasColpatria.Models.Reportes;

    public class Contexto : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'Contexto' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'tmkhogares.Models.Contexto' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'Contexto'  en el archivo de configuración de la aplicación.
        public Contexto()
            : base("name=Contexto")
        {
        }


        /*
         * MMODEL BILDER
         */
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Entity>()
        //    .HasIndex(p => new { p.FirstColumn, p.SecondColumn }).IsUnique();
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }




        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.


        #region CONFIGURATIONS
        public DbSet<Category> Categories { get; set; }
        public DbSet<Configuration> Configurations { get; set; }

        #endregion






        #region USUARIOS
        public DbSet<User.User> Usuario { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<UserRol> UsuariosRoles { get; set; }
        #endregion

        #region DASHBOARD
        public DbSet<Dashboard> Dashboard { get; set; }

        #endregion

        #region REPORTES

        public DbSet<Reportes> Reportes { get; set; }
        //public DbSet<UserReports> UserReports { get; set; }
        public DbSet<campanas> campanas { get; set; }
        public DbSet<TipoReporte> tiporeporte { get; set; }
        public DbSet<Logo> Logos { get; set; }


        #endregion

        #region PERMISOS

        public DbSet<PermisosDash> permisosdash { get; set; }


        #endregion


        #region AREAS
        public DbSet<Areas> areas { get; set;}

        #endregion
    }



}