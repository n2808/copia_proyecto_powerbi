﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TarjetasColpatria.Models.Permisos;

namespace Core.Models.User
{
	public class User : Entity
	{
        [DisplayName("Documento")]
		public string Document { get; set; }
        [DisplayName("Nombres")]
        public string Names { get; set; }
        [DisplayName("Apellidos")]
        public string LastName { get; set; }
        [DisplayName("Telefono 1")]
        public string Phone1 { get; set; }
        [DisplayName("Telefono 2")]
        public string Phone2 { get; set; }
        [DisplayName("Telefono 3")]
        public string Phone3 { get; set; }
        [DisplayName("Correo")]
        public string Email { get; set; }
        [DisplayName("Estado")]
        public bool Status { get; set; }
        [DisplayName("password")]
        public string PassWord { get; set; }

        public string login { get; set; }

        public List<UserRol> UserRol { get; set; }

  
        public Guid? CoordinadorId { get; set; }

        [ForeignKey("CoordinadorId")]
        public User Coordinador { get; set; }

      
       
    }
}