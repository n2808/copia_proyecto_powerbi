﻿using Microsoft.PowerBI.Api.V2.Models;
using System.ComponentModel.DataAnnotations;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PowerBIEmbedded_AppOwnsData.Models
{
    public class EmbedConfig
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Id { get; set; }

        public string EmbedUrl { get; set; }

        public EmbedToken EmbedToken { get; set; }

        public int MinutesToExpiration
        {
            get
            {
                var minutesToExpiration = EmbedToken.Expiration.Value - DateTime.UtcNow;
                return minutesToExpiration.Minutes;
            }
        }

        public bool? IsEffectiveIdentityRolesRequired { get; set; }

        public bool? IsEffectiveIdentityRequired { get; set; }

        public bool EnableRLS { get; set; }

        public string Username { get; set; }

        public string Roles { get; set; }

        public string ErrorMessage { get; internal set; }

        //[Required] //Dato requerido
        //[EmailAddress] //Validar que se ingrese un email valido
        //[StringLength(150)] //longitud maxima del campo
        //[Display(Name = "Email address ")] //Mensaje indicar obligatorio

       

        //[Required] //Dato requerido
        //[DataType(DataType.Password)] //Indicar dato  tipo password
        //[StringLength(20, MinimumLength = 6)] //Longitud minima y maxima
        //[Display(Name = "Password ")] //Mensaje indicar obligatorio

        
    }
    
}