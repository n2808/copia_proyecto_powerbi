﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Models.Security
{
	public class Accounts
	{
		public Guid? Id { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string[] Roles { get; set; }
        public List<ReportsPermisos> Permisos { get; set; }
	}

    public class ReportsPermisos
    {
        public Guid Id { get; set; }
        public string Campana { get; set; }
        public string DashBoard { get; set; }
        public string Nombre { get; set; }

        

    }
}