﻿using PowerBi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Core.Models.Security
{
  //  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class CustomAuthorizeAttribute : AuthorizeAttribute
	{
        private User.User usuario;       
        private int operacion;
        private Contexto db = new Contexto();

        //public CustomAuthorizeAttribute(int operacion = 0)
        //{
        //    this.operacion = operacion;
        //}


        public override void OnAuthorization(AuthorizationContext filterContext)
        {                 

            //string nombreOperacion = "";
            //string nombremodulo = "";
            //try
            //{
            //    usuario = (User.User)HttpContext.Current.Session["Users"];
            //    var users = from m in db.permisosdash
            //                where m.PermisosUsers ==usuario.Id
            //                select m;

            //    if (users.ToList().Count() == 0)
            //    {
            //        filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { Controller = "Accounts", action = "AccessDenied" }));
            //    }

            //}
            //catch(Exception ex)
            //{
            //    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { Controller = "Accounts", action = "AccessDenied" }));
            //}
        
                                 
            //SessionPersister.UserName = "USUARIO DE PRUEBA CRISTIAN";
            //SessionPersister.Id = new Guid("74F53F27-5344-49B0-91BD-21A295F1EACC");
            HttpContext.Current.Session.Timeout = 24 * 60;
            if (SessionPersister.Id == null)
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Accounts", action = "index" }));
            else
            {
                if (Roles != "")
                {


                    AccountsModel am = new AccountsModel();
                    CustomPrincipal mp = new CustomPrincipal(am.find(SessionPersister.Id));
                    if (!mp.IsInRole(Roles))
                        filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { Controller = "Accounts", action = "AccessDenied" }));
                }
            }

        }      //base.OnAuthorization(filterContext);
		
	}
}