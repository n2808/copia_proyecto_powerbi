﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TarjetasColpatria.Models.Reportes;

namespace TarjetasColpatria.Models.Dashboard
{
    public class Dashboard: Entity
    {

        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Estado")]
        public Boolean Estado { get; set; }

       
      
        [DisplayName("Campaña")]
        public Guid CampanaId { get; set; }


         //Foraneas

        [ForeignKey("CampanaId")]
        public campanas campana { get; set; }
   

    }
}