﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TarjetasColpatria.Models
{
    public class campanas : Entity
    {
        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Estado")]
        public Boolean Estado { get; set; }

        public Guid? Areas { get; set; }

        //foraneas        
        [ForeignKey ("Areas")]
        [DisplayName("Area")]
        public Areas.Areas AreasId { get; set; }
      

    }
}