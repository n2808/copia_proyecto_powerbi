﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TarjetasColpatria.Models.Permisos
{
    public class PermisosDash:Entity
    {
        [DisplayName("Campaña")]
        public Guid? Campana { get; set; }

        [DisplayName("Dashboards")]
        public Guid? Dashboard { get; set; }

        [DisplayName("Reportes")]
        public Guid? Reportes { get; set; }

        public Guid? PermisosUsers { get; set; }


        public Boolean? PermisodeDashboard { get; set; }
        public Boolean? campanaPermiso { get; set; }

  
        //llaves foraneas

        [ForeignKey("Campana")]
        public campanas campanaId { get; set; }

        [ForeignKey("Dashboard")]
        public Dashboard.Dashboard dashboardId { get; set; }


        [ForeignKey("Reportes")]
        public Reportes.Reportes reportesId { get; set; }
        

        [ForeignKey("PermisosUsers")]
        public User PermisosuserId { get; set; }
       
    }
}