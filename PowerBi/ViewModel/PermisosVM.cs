﻿using Core.Models.User;
using Microsoft.PowerBI.Api.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TarjetasColpatria.Models;
using TarjetasColpatria.Models.Reportes;

namespace TarjetasColpatria.ViewModel
{
    public class PermisosVM
    {


    }


    public class UsersPermisosVM
    {

        public Guid? Id { get; set; }
        public string campana { get; set; }
        public Guid? dashboards { get; set; }
        public Guid? reportes { get; set; }
        public string usuarios { get; set; }
        public string apellido { get; set; }
        public string documento { get; set; }

    }
}