﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.ViewModel
{
    public class CustoErrors
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public CustomErrorType Type { get; set; }
    }

    public enum CustomErrorType
    {
        Simple = 0,
        Nofound = 404,
        badRequest = 500
    }

    // Estratos
    public static class _Estratos
    {
        public static Guid Estrato1 { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");
        public static Guid Estrato2 { get; set; } = new Guid("3901d691-2f04-4260-a613-4bea542bf053");
        public static Guid Estrato3 { get; set; } = new Guid("303396dd-26a5-4ed5-8de6-c71f2cb27b8a");
        public static Guid Estrato4 { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");
        public static Guid Estrato5 { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");
        public static Guid Estrato6 { get; set; } = new Guid("65f56be4-1a43-438d-8f1c-1feebd6733eb");
    }
    // estados de las gestiones
    public static class _EstadosGestion
    {
        // venta ok
        public static Guid Realce { get; set; } = new Guid("fbe84570-8692-47f0-b44c-3f239577c1a3");
        public static Guid Logistica { get; set; } = new Guid("1feb41f9-593c-451f-9d0d-418b260eb325");
        public static Guid Activacion { get; set; } = new Guid("7ada6687-c83a-4780-9e8c-4eeb6116dd82");



        public static Guid EnGestion { get; set; } = new Guid("46E6DB07-510A-4715-81AC-40CC4F7733BC");
        public static Guid NoVenta { get; set; } = new Guid("9B6B7E0B-1256-4842-9F46-165555C202E8");
        public static Guid Preventa { get; set; } = new Guid("785e5148-50f7-4c43-a7f3-23a8c9c68f0b");


    }

    /*
     * ROLES DE LA APLICACIÓN
     */

    public static class _Roles
    {
        public static Guid Asesor { get; set; } = new Guid("27c3d61f-aefe-40ce-8eaa-3292117fa4b6");
        public static Guid Validador { get; set; } = new Guid("5f0614f8-65e8-43f9-96a1-1bebcbaf549d");
        public static Guid Administrador { get; set; } = new Guid("520469d8-724c-4cb6-96c2-45001209a39b");

    }

    /*
     * LISTA DE CONFIGURACIONES 
     */
    public static class _Configuraciones
    {


        public static Guid Profesiones { get; set; } = new Guid("f3d3e00c-116b-4875-80d3-c80852231acb");
        public static Guid ActividadEconomica { get; set; } = new Guid("2808e62b-6ab8-415d-b9d8-ec4c7d237803");



    }

    /*
     * LISTA DE GESTIONES 
     */

    public static class _Tipificaciones
    {

        //  VENTAS
        public static int VentaFijo1 { get; set; } = 25;
        public static int VentaFijo2 { get; set; } = 67;
        public static int VentaFijo3 { get; set; } = 108;
        public static int VentaMovil1 { get; set; } = 26;
        public static int VentaMovil2 { get; set; } = 68;
        public static int VentaMovil3 { get; set; } = 109;
        public static int VentaMultiplay1 { get; set; } = 27;
        public static int VentaMultiplay2 { get; set; } = 69;
        public static int VentaMultiplay3 { get; set; } = 110;

        //PREVENTAS PADRES
        public static int Preventa1 { get; set; } = 170;
        public static int Preventa2 { get; set; } = 172;
        public static int Preventa3 { get; set; } = 174;

        //PREVENTAS
        public static int PreventaHPP1 { get; set; } = 171;
        public static int PreventaHPP2 { get; set; } = 173;
        public static int PreventaHPP3 { get; set; } = 175;

        public static List<int> LPreventas { get; set; } = new List<int>()
        {
            PreventaHPP1,
            PreventaHPP2,
            PreventaHPP3
        };
    }

    public static class _EstadoCliente
    {
        public static Guid Activo { get; set; } = new Guid("1eb77c67-16ff-4247-b875-001f055f4854");
        public static Guid Inactivo { get; set; } = new Guid("afc34a8d-4885-422a-b13a-b2b1079a5c6e");
    }

   

}