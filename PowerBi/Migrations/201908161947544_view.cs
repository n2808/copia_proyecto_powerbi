namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class view : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Permisos", "Campana", "dbo.campanas");
            DropForeignKey("dbo.Permisos", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.Permisos", "Reportes", "dbo.Reportes");
            DropForeignKey("dbo.Permisos", "TipoPermiso", "dbo.TipoPermisos");
            DropIndex("dbo.Permisos", new[] { "Campana" });
            DropIndex("dbo.Permisos", new[] { "Dashboard" });
            DropIndex("dbo.Permisos", new[] { "Reportes" });
            DropIndex("dbo.Permisos", new[] { "TipoPermiso" });
            DropTable("dbo.Permisos");
            DropTable("dbo.TipoPermisos");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TipoPermisos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Permisos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Campana = c.Guid(nullable: false),
                        Dashboard = c.Guid(nullable: false),
                        Reportes = c.Guid(nullable: false),
                        TipoPermiso = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Permisos", "TipoPermiso");
            CreateIndex("dbo.Permisos", "Reportes");
            CreateIndex("dbo.Permisos", "Dashboard");
            CreateIndex("dbo.Permisos", "Campana");
            AddForeignKey("dbo.Permisos", "TipoPermiso", "dbo.TipoPermisos", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "Reportes", "dbo.Reportes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "Dashboard", "dbo.Dashboards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "Campana", "dbo.campanas", "Id", cascadeDelete: true);
        }
    }
}
