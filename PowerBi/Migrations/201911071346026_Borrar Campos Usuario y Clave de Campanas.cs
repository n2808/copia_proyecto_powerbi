namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BorrarCamposUsuarioyClavedeCampanas : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.campanas", "Usuario");
            DropColumn("dbo.campanas", "Clave");
        }
        
        public override void Down()
        {
            AddColumn("dbo.campanas", "Clave", c => c.String());
            AddColumn("dbo.campanas", "Usuario", c => c.String());
        }
    }
}
