namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class organizaciondedatos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dashboards", "ReporteId", "dbo.Reportes");
            DropIndex("dbo.Dashboards", new[] { "ReporteId" });
            AlterColumn("dbo.Dashboards", "ReporteId", c => c.Guid());
            AlterColumn("dbo.Dashboards", "DashboardId", c => c.Guid());
            AlterColumn("dbo.Reportes", "DashboardId", c => c.Guid());
            CreateIndex("dbo.Dashboards", "ReporteId");
            AddForeignKey("dbo.Dashboards", "ReporteId", "dbo.Reportes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dashboards", "ReporteId", "dbo.Reportes");
            DropIndex("dbo.Dashboards", new[] { "ReporteId" });
            AlterColumn("dbo.Reportes", "DashboardId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Dashboards", "DashboardId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Dashboards", "ReporteId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Dashboards", "ReporteId");
            AddForeignKey("dbo.Dashboards", "ReporteId", "dbo.Reportes", "Id", cascadeDelete: true);
        }
    }
}
