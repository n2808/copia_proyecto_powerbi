namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secreatablacampanaysellavesfraneas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.campanas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Dashboards", "CampanaId");
            CreateIndex("dbo.Dashboards", "ReporteId");
            AddForeignKey("dbo.Dashboards", "CampanaId", "dbo.campanas", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Dashboards", "ReporteId", "dbo.Reportes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dashboards", "ReporteId", "dbo.Reportes");
            DropForeignKey("dbo.Dashboards", "CampanaId", "dbo.campanas");
            DropIndex("dbo.Dashboards", new[] { "ReporteId" });
            DropIndex("dbo.Dashboards", new[] { "CampanaId" });
            DropTable("dbo.campanas");
        }
    }
}
