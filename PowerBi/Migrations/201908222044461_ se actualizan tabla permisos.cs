namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seactualizantablapermisos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PermisosDashes", "Campana", "dbo.campanas");
            DropForeignKey("dbo.PermisosDashes", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.PermisosDashes", "Reportes", "dbo.Reportes");
            DropIndex("dbo.PermisosDashes", new[] { "Campana" });
            DropIndex("dbo.PermisosDashes", new[] { "Dashboard" });
            DropIndex("dbo.PermisosDashes", new[] { "Reportes" });
            AlterColumn("dbo.PermisosDashes", "Campana", c => c.Guid());
            AlterColumn("dbo.PermisosDashes", "Dashboard", c => c.Guid());
            AlterColumn("dbo.PermisosDashes", "Reportes", c => c.Guid());
            CreateIndex("dbo.PermisosDashes", "Campana");
            CreateIndex("dbo.PermisosDashes", "Dashboard");
            CreateIndex("dbo.PermisosDashes", "Reportes");
            AddForeignKey("dbo.PermisosDashes", "Campana", "dbo.campanas", "Id");
            AddForeignKey("dbo.PermisosDashes", "Dashboard", "dbo.Dashboards", "Id");
            AddForeignKey("dbo.PermisosDashes", "Reportes", "dbo.Reportes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PermisosDashes", "Reportes", "dbo.Reportes");
            DropForeignKey("dbo.PermisosDashes", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.PermisosDashes", "Campana", "dbo.campanas");
            DropIndex("dbo.PermisosDashes", new[] { "Reportes" });
            DropIndex("dbo.PermisosDashes", new[] { "Dashboard" });
            DropIndex("dbo.PermisosDashes", new[] { "Campana" });
            AlterColumn("dbo.PermisosDashes", "Reportes", c => c.Guid(nullable: false));
            AlterColumn("dbo.PermisosDashes", "Dashboard", c => c.Guid(nullable: false));
            AlterColumn("dbo.PermisosDashes", "Campana", c => c.Guid(nullable: false));
            CreateIndex("dbo.PermisosDashes", "Reportes");
            CreateIndex("dbo.PermisosDashes", "Dashboard");
            CreateIndex("dbo.PermisosDashes", "Campana");
            AddForeignKey("dbo.PermisosDashes", "Reportes", "dbo.Reportes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PermisosDashes", "Dashboard", "dbo.Dashboards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PermisosDashes", "Campana", "dbo.campanas", "Id", cascadeDelete: true);
        }
    }
}
