namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removerDashboardIdfromdashboard : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Dashboards", "DashboardId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dashboards", "DashboardId", c => c.String());
        }
    }
}
