namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualizacion : DbMigration
    {
        public override void Up()
        {
            //RenameColumn(table: "dbo.Users", name: "Usuariopermiso", newName: "permisos");
            //RenameIndex(table: "dbo.Users", name: "IX_Usuariopermiso", newName: "IX_permisos");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Users", name: "IX_permisos", newName: "IX_Usuariopermiso");
            RenameColumn(table: "dbo.Users", name: "permisos", newName: "Usuariopermiso");
        }
    }
}
