namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secambiancamposdepermisosaboolean : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PermisosDashes", "PermisodeDashboard", c => c.Boolean());
            AddColumn("dbo.PermisosDashes", "PermisosdeUsers", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PermisosDashes", "PermisosdeUsers");
            DropColumn("dbo.PermisosDashes", "PermisodeDashboard");
        }
    }
}
