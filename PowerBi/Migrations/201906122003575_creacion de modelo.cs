namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creaciondemodelo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Status = c.Boolean(nullable: false),
                        Description = c.String(),
                        CanChanged = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Configurations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        OrderId = c.Int(nullable: false),
                        Description = c.String(),
                        Status = c.Boolean(nullable: false),
                        CategoriesId = c.Guid(nullable: false),
                        GuidId = c.Guid(),
                        IntId = c.Int(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoriesId, cascadeDelete: false)
                .Index(t => t.CategoriesId);
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Document = c.String(),
                        Names = c.String(),
                        LastName = c.String(),
                        Phone1 = c.String(),
                        Phone2 = c.String(),
                        Phone3 = c.String(),
                        Email = c.String(),
                        Status = c.Boolean(nullable: false),
                        PassWord = c.String(),
                        login = c.String(),
                        CoordinadorId = c.Guid(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CoordinadorId)
                .Index(t => t.CoordinadorId);
            
            CreateTable(
                "dbo.UserRols",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RolId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rols", t => t.RolId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.RolId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRols", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRols", "RolId", "dbo.Rols");
            DropForeignKey("dbo.Users", "CoordinadorId", "dbo.Users");
            DropForeignKey("dbo.Configurations", "CategoriesId", "dbo.Categories");
            DropIndex("dbo.UserRols", new[] { "UserId" });
            DropIndex("dbo.UserRols", new[] { "RolId" });
            DropIndex("dbo.Users", new[] { "CoordinadorId" });
            DropIndex("dbo.Configurations", new[] { "CategoriesId" });
            DropTable("dbo.UserRols");
            DropTable("dbo.Users");
            DropTable("dbo.Rols");
            DropTable("dbo.Configurations");
            DropTable("dbo.Categories");
        }
    }
}
