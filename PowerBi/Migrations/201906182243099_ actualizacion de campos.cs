namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualizaciondecampos : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reportes", "DashboardId", c => c.Guid());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reportes", "DashboardId", c => c.Guid(nullable: false));
        }
    }
}
