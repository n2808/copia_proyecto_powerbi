namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtablepermisosdash : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PermisosDashes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Campana = c.Guid(),
                        Dashboard = c.Guid(),
                        Reportes = c.Guid(),
                        PermisosUsers = c.Guid(),
                        PermisodeDashboard = c.Boolean(),
                        campanaPermiso = c.Boolean(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.campanas", t => t.Campana)
                .ForeignKey("dbo.Dashboards", t => t.Dashboard)
                .ForeignKey("dbo.Users", t => t.PermisosUsers)
                .ForeignKey("dbo.Reportes", t => t.Reportes)
                .Index(t => t.Campana)
                .Index(t => t.Dashboard)
                .Index(t => t.Reportes)
                .Index(t => t.PermisosUsers);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PermisosDashes", "Reportes", "dbo.Reportes");
            DropForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users");
            DropForeignKey("dbo.PermisosDashes", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.PermisosDashes", "Campana", "dbo.campanas");
            DropIndex("dbo.PermisosDashes", new[] { "PermisosUsers" });
            DropIndex("dbo.PermisosDashes", new[] { "Reportes" });
            DropIndex("dbo.PermisosDashes", new[] { "Dashboard" });
            DropIndex("dbo.PermisosDashes", new[] { "Campana" });
            DropTable("dbo.PermisosDashes");
        }
    }
}
