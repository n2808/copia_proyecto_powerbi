namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secreallavesforaneasdepermisos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permisos",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Campana = c.Guid(nullable: false),
                    Dashboard = c.Guid(nullable: false),
                    Reportes = c.Guid(nullable: false),
                    TipoPermiso = c.Guid(nullable: false),
                    PermisoCampana = c.Guid(nullable: false),
                    PermisoDashboard = c.Guid(nullable: false),
                    CreatedAt = c.DateTime(),
                    UpdatedAt = c.DateTime(),
                    DeletedAt = c.DateTime(),
                    CreatedBy = c.Guid(),
                    UpdatedBy = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.campanas", t => t.Campana, cascadeDelete: true)
                .ForeignKey("dbo.Dashboards", t => t.Dashboard, cascadeDelete: true)
                .ForeignKey("dbo.campanas", t => t.PermisoCampana, cascadeDelete: true)
                .ForeignKey("dbo.Dashboards", t => t.PermisoDashboard, cascadeDelete: true)
                .ForeignKey("dbo.Reportes", t => t.Reportes, cascadeDelete: true)
                .ForeignKey("dbo.TipoPermisos", t => t.TipoPermiso, cascadeDelete: true)
                .Index(t => t.Campana)
                .Index(t => t.Dashboard)
                .Index(t => t.Reportes)
                .Index(t => t.TipoPermiso)
                .Index(t => t.PermisoCampana)
                .Index(t => t.PermisoDashboard);

            CreateTable(
                "dbo.TipoPermisos",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Nombre = c.String(),
                    Estado = c.Boolean(nullable: false),
                    CreatedAt = c.DateTime(),
                    UpdatedAt = c.DateTime(),
                    DeletedAt = c.DateTime(),
                    CreatedBy = c.Guid(),
                    UpdatedBy = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            AddColumn("dbo.Users", "permisos", c => c.Guid(nullable: false));
            CreateIndex("dbo.Users", "permisos");
            AddForeignKey("dbo.Users", "permisos", "dbo.Permisos", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "permisos", "dbo.Permisos");
            DropForeignKey("dbo.Permisos", "TipoPermiso", "dbo.TipoPermisos");
            DropForeignKey("dbo.Permisos", "Reportes", "dbo.Reportes");
            DropForeignKey("dbo.Permisos", "PermisoDashboard", "dbo.Dashboards");
            DropForeignKey("dbo.Permisos", "PermisoCampana", "dbo.campanas");
            DropForeignKey("dbo.Permisos", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.Permisos", "Campana", "dbo.campanas");
            DropIndex("dbo.Users", new[] { "permisos" });
            DropIndex("dbo.Permisos", new[] { "PermisoDashboard" });
            DropIndex("dbo.Permisos", new[] { "PermisoCampana" });
            DropIndex("dbo.Permisos", new[] { "TipoPermiso" });
            DropIndex("dbo.Permisos", new[] { "Reportes" });
            DropIndex("dbo.Permisos", new[] { "Dashboard" });
            DropIndex("dbo.Permisos", new[] { "Campana" });
            DropColumn("dbo.Users", "permisos");
            DropTable("dbo.TipoPermisos");
            DropTable("dbo.Permisos");
        }
    }
}
