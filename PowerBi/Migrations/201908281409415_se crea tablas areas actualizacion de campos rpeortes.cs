namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secreatablasareasactualizaciondecamposrpeortes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        descripcion = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TipoReportes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.campanas", "Areas", c => c.Guid());
            AddColumn("dbo.Reportes", "area", c => c.Guid(nullable: false));
            AddColumn("dbo.Reportes", "campana", c => c.Guid(nullable: false));
            AddColumn("dbo.Reportes", "WORKGROUP_NAME", c => c.String());
            AddColumn("dbo.Reportes", "WORKGROUP_ID", c => c.String());
            AddColumn("dbo.Reportes", "REPORT_NAME", c => c.String());
            AddColumn("dbo.Reportes", "REPORT_ID", c => c.String());
            AddColumn("dbo.Reportes", "REPORT_FILTER", c => c.String());
            AddColumn("dbo.Reportes", "TIPO_REPORTE", c => c.Guid(nullable: false));
            AddColumn("dbo.Reportes", "REPORT_DETALLE", c => c.String());
            CreateIndex("dbo.campanas", "Areas");
            CreateIndex("dbo.Reportes", "area");
            CreateIndex("dbo.Reportes", "campana");
            CreateIndex("dbo.Reportes", "TIPO_REPORTE");
            AddForeignKey("dbo.campanas", "Areas", "dbo.Areas", "Id");
            //AddForeignKey("dbo.Reportes", "area", "dbo.Areas", "Id", cascadeDelete: false);
            //AddForeignKey("dbo.Reportes", "campana", "dbo.campanas", "Id", cascadeDelete: false);
            //AddForeignKey("dbo.Reportes", "TIPO_REPORTE", "dbo.TipoReportes", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reportes", "TIPO_REPORTE", "dbo.TipoReportes");
            DropForeignKey("dbo.Reportes", "campana", "dbo.campanas");
            DropForeignKey("dbo.Reportes", "area", "dbo.Areas");
            DropForeignKey("dbo.campanas", "Areas", "dbo.Areas");
            DropIndex("dbo.Reportes", new[] { "TIPO_REPORTE" });
            DropIndex("dbo.Reportes", new[] { "campana" });
            DropIndex("dbo.Reportes", new[] { "area" });
            DropIndex("dbo.campanas", new[] { "Areas" });
            DropColumn("dbo.Reportes", "REPORT_DETALLE");
            DropColumn("dbo.Reportes", "TIPO_REPORTE");
            DropColumn("dbo.Reportes", "REPORT_FILTER");
            DropColumn("dbo.Reportes", "REPORT_ID");
            DropColumn("dbo.Reportes", "REPORT_NAME");
            DropColumn("dbo.Reportes", "WORKGROUP_ID");
            DropColumn("dbo.Reportes", "WORKGROUP_NAME");
            DropColumn("dbo.Reportes", "campana");
            DropColumn("dbo.Reportes", "area");
            DropColumn("dbo.campanas", "Areas");
            DropTable("dbo.TipoReportes");
            DropTable("dbo.Areas");
        }
    }
}
