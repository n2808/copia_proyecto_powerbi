namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foraneasdepermisos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PermisosDashes", "PermisoCampana", "dbo.Reportes");
            DropForeignKey("dbo.PermisosDashes", "PermisoDashboard", "dbo.Reportes");
            DropForeignKey("dbo.PermisosDashes", "Permisos", "dbo.Reportes");
            DropIndex("dbo.PermisosDashes", new[] { "Permisos" });
            DropIndex("dbo.PermisosDashes", new[] { "PermisoCampana" });
            DropIndex("dbo.PermisosDashes", new[] { "PermisoDashboard" });
            AlterColumn("dbo.PermisosDashes", "Permisos", c => c.Guid());
            AlterColumn("dbo.PermisosDashes", "PermisoCampana", c => c.Guid());
            AlterColumn("dbo.PermisosDashes", "PermisoDashboard", c => c.Guid());
            CreateIndex("dbo.PermisosDashes", "Permisos");
            CreateIndex("dbo.PermisosDashes", "PermisoCampana");
            CreateIndex("dbo.PermisosDashes", "PermisoDashboard");
            AddForeignKey("dbo.PermisosDashes", "PermisoCampana", "dbo.Reportes", "Id");
            AddForeignKey("dbo.PermisosDashes", "PermisoDashboard", "dbo.Reportes", "Id");
            AddForeignKey("dbo.PermisosDashes", "Permisos", "dbo.Reportes", "Id");
            DropColumn("dbo.PermisosDashes", "PermisoTipo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PermisosDashes", "PermisoTipo", c => c.Guid(nullable: false));
            DropForeignKey("dbo.PermisosDashes", "Permisos", "dbo.Reportes");
            DropForeignKey("dbo.PermisosDashes", "PermisoDashboard", "dbo.Reportes");
            DropForeignKey("dbo.PermisosDashes", "PermisoCampana", "dbo.Reportes");
            DropIndex("dbo.PermisosDashes", new[] { "PermisoDashboard" });
            DropIndex("dbo.PermisosDashes", new[] { "PermisoCampana" });
            DropIndex("dbo.PermisosDashes", new[] { "Permisos" });
            AlterColumn("dbo.PermisosDashes", "PermisoDashboard", c => c.Guid(nullable: false));
            AlterColumn("dbo.PermisosDashes", "PermisoCampana", c => c.Guid(nullable: false));
            AlterColumn("dbo.PermisosDashes", "Permisos", c => c.Guid(nullable: false));
            CreateIndex("dbo.PermisosDashes", "PermisoDashboard");
            CreateIndex("dbo.PermisosDashes", "PermisoCampana");
            CreateIndex("dbo.PermisosDashes", "Permisos");
            AddForeignKey("dbo.PermisosDashes", "Permisos", "dbo.Reportes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PermisosDashes", "PermisoDashboard", "dbo.Reportes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PermisosDashes", "PermisoCampana", "dbo.Reportes", "Id", cascadeDelete: true);
        }
    }
}
