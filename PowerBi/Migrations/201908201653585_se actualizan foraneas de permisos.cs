namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seactualizanforaneasdepermisos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Permisos", "Campana", "dbo.campanas");
            DropForeignKey("dbo.Permisos", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.Permisos", "PermisoCampana", "dbo.campanas");
            DropForeignKey("dbo.Permisos", "PermisoDashboard", "dbo.Dashboards");
            DropForeignKey("dbo.Permisos", "Reportes", "dbo.Reportes");
            DropForeignKey("dbo.Permisos", "TipoPermiso", "dbo.TipoPermisos");
            DropForeignKey("dbo.PermisosDashes", "PermisoTipo", "dbo.PermisosTipoes");
            DropIndex("dbo.Permisos", new[] { "Campana" });
            DropIndex("dbo.Permisos", new[] { "Dashboard" });
            DropIndex("dbo.Permisos", new[] { "Reportes" });
            DropIndex("dbo.Permisos", new[] { "TipoPermiso" });
            DropIndex("dbo.Permisos", new[] { "PermisoCampana" });
            DropIndex("dbo.Permisos", new[] { "PermisoDashboard" });
            DropIndex("dbo.PermisosDashes", new[] { "PermisoTipo" });
            AddColumn("dbo.PermisosDashes", "Permisos", c => c.Guid(nullable: false));
            CreateIndex("dbo.PermisosDashes", "Permisos");
            AddForeignKey("dbo.PermisosDashes", "Permisos", "dbo.Reportes", "Id", cascadeDelete: false);
            DropColumn("dbo.PermisosDashes", "TipoPermiso");
           // DropTable("dbo.Permisos");
          //  DropTable("dbo.TipoPermisos");
            DropTable("dbo.PermisosTipoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PermisosTipoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TipoPermisos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Permisos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Campana = c.Guid(nullable: false),
                        Dashboard = c.Guid(nullable: false),
                        Reportes = c.Guid(nullable: false),
                        TipoPermiso = c.Guid(nullable: false),
                        PermisoCampana = c.Guid(nullable: false),
                        PermisoDashboard = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.PermisosDashes", "TipoPermiso", c => c.Guid(nullable: false));
            DropForeignKey("dbo.PermisosDashes", "Permisos", "dbo.Reportes");
            DropIndex("dbo.PermisosDashes", new[] { "Permisos" });
            DropColumn("dbo.PermisosDashes", "Permisos");
            CreateIndex("dbo.PermisosDashes", "PermisoTipo");
            CreateIndex("dbo.Permisos", "PermisoDashboard");
            CreateIndex("dbo.Permisos", "PermisoCampana");
            CreateIndex("dbo.Permisos", "TipoPermiso");
            CreateIndex("dbo.Permisos", "Reportes");
            CreateIndex("dbo.Permisos", "Dashboard");
            CreateIndex("dbo.Permisos", "Campana");
            AddForeignKey("dbo.PermisosDashes", "PermisoTipo", "dbo.PermisosTipoes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "TipoPermiso", "dbo.TipoPermisos", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "Reportes", "dbo.Reportes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "PermisoDashboard", "dbo.Dashboards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "PermisoCampana", "dbo.campanas", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "Dashboard", "dbo.Dashboards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permisos", "Campana", "dbo.campanas", "Id", cascadeDelete: true);
        }
    }
}
