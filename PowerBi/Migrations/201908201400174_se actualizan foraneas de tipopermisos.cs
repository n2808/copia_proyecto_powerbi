namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seactualizanforaneasdetipopermisos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PermisosDashes", "TipoPermiso", "dbo.TipoPermisos");
            DropIndex("dbo.PermisosDashes", new[] { "TipoPermiso" });
            AddColumn("dbo.PermisosDashes", "PermisoTipo", c => c.Guid(nullable: false));
            CreateIndex("dbo.PermisosDashes", "PermisoTipo");
            AddForeignKey("dbo.PermisosDashes", "PermisoTipo", "dbo.PermisosTipoes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PermisosDashes", "PermisoTipo", "dbo.PermisosTipoes");
            DropIndex("dbo.PermisosDashes", new[] { "PermisoTipo" });
            DropColumn("dbo.PermisosDashes", "PermisoTipo");
            CreateIndex("dbo.PermisosDashes", "TipoPermiso");
            AddForeignKey("dbo.PermisosDashes", "TipoPermiso", "dbo.TipoPermisos", "Id", cascadeDelete: true);
        }
    }
}
