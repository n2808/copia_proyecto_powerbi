namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secreacampourlenreportes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reportes", "Url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reportes", "Url");
        }
    }
}
