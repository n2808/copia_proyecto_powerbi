namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actaulzaciondecambios : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dashboards", "reportes", "dbo.Reportes");
            DropForeignKey("dbo.campanas", "dashboard", "dbo.Dashboards");
            DropIndex("dbo.campanas", new[] { "dashboard" });
            DropIndex("dbo.Dashboards", new[] { "reportes" });
            CreateIndex("dbo.Dashboards", "CampanaId");
            CreateIndex("dbo.Reportes", "DashboardId");
            AddForeignKey("dbo.Dashboards", "CampanaId", "dbo.campanas", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Reportes", "DashboardId", "dbo.Dashboards", "Id");
            DropColumn("dbo.campanas", "dashboard");
            DropColumn("dbo.Dashboards", "reportes");
            DropColumn("dbo.Reportes", "Url");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reportes", "Url", c => c.String());
            AddColumn("dbo.Dashboards", "reportes", c => c.Guid(nullable: false));
            AddColumn("dbo.campanas", "dashboard", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Reportes", "DashboardId", "dbo.Dashboards");
            DropForeignKey("dbo.Dashboards", "CampanaId", "dbo.campanas");
            DropIndex("dbo.Reportes", new[] { "DashboardId" });
            DropIndex("dbo.Dashboards", new[] { "CampanaId" });
            CreateIndex("dbo.Dashboards", "reportes");
            CreateIndex("dbo.campanas", "dashboard");
            AddForeignKey("dbo.campanas", "dashboard", "dbo.Dashboards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Dashboards", "reportes", "dbo.Reportes", "Id", cascadeDelete: true);
        }
    }
}
