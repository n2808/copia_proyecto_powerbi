namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seelimacampodereporte : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Dashboards", "ReporteId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dashboards", "ReporteId", c => c.Guid());
        }
    }
}
