namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secreatablapermisosytipopermisos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permisos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Campana = c.Guid(nullable: false),
                        Dashboard = c.Guid(nullable: false),
                        Reportes = c.Guid(nullable: false),
                        TipoPermiso = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.campanas", t => t.Campana, cascadeDelete: true)
                .ForeignKey("dbo.Dashboards", t => t.Dashboard, cascadeDelete: false)
                .ForeignKey("dbo.Reportes", t => t.Reportes, cascadeDelete: true)
                .ForeignKey("dbo.TipoPermisos", t => t.TipoPermiso, cascadeDelete: true)
                .Index(t => t.Campana)
                .Index(t => t.Dashboard)
                .Index(t => t.Reportes)
                .Index(t => t.TipoPermiso);
            
            CreateTable(
                "dbo.TipoPermisos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Permisos", "TipoPermiso", "dbo.TipoPermisos");
            DropForeignKey("dbo.Permisos", "Reportes", "dbo.Reportes");
            DropForeignKey("dbo.Permisos", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.Permisos", "Campana", "dbo.campanas");
            DropIndex("dbo.Permisos", new[] { "TipoPermiso" });
            DropIndex("dbo.Permisos", new[] { "Reportes" });
            DropIndex("dbo.Permisos", new[] { "Dashboard" });
            DropIndex("dbo.Permisos", new[] { "Campana" });
            DropTable("dbo.TipoPermisos");
            DropTable("dbo.Permisos");
        }
    }
}
