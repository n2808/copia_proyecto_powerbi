namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seactualizanforaneadepermisos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "permisos", "dbo.PermisosDashes");
            DropIndex("dbo.Users", new[] { "permisos" });
            AddColumn("dbo.PermisosDashes", "PermisosUsers", c => c.Guid(nullable: false));
            CreateIndex("dbo.PermisosDashes", "PermisosUsers");
            AddForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users", "Id", cascadeDelete: true);
            DropColumn("dbo.Users", "permisos");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "permisos", c => c.Guid(nullable: false));
            DropForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users");
            DropIndex("dbo.PermisosDashes", new[] { "PermisosUsers" });
            DropColumn("dbo.PermisosDashes", "PermisosUsers");
            CreateIndex("dbo.Users", "permisos");
            AddForeignKey("dbo.Users", "permisos", "dbo.PermisosDashes", "Id", cascadeDelete: true);
        }
    }
}
