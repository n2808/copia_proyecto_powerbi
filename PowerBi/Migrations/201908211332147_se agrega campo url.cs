namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagregacampourl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reportes", "url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reportes", "url");
        }
    }
}
