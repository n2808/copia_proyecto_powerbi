namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creaciondelatabla : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dashboards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        estado = c.Boolean(nullable: false),
                        DashboardId = c.String(),
                        campanaId = c.String(),
                        ReporteId = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Dashboards");
        }
    }
}
