namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eliminarlatablapermisosdash : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PermisosDashes", "Campana", "dbo.campanas");
            DropForeignKey("dbo.PermisosDashes", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users");
            DropForeignKey("dbo.PermisosDashes", "Reportes", "dbo.Reportes");
            DropIndex("dbo.PermisosDashes", new[] { "Campana" });
            DropIndex("dbo.PermisosDashes", new[] { "Dashboard" });
            DropIndex("dbo.PermisosDashes", new[] { "Reportes" });
            DropIndex("dbo.PermisosDashes", new[] { "PermisosUsers" });
            DropTable("dbo.PermisosDashes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PermisosDashes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Campana = c.Guid(),
                        Dashboard = c.Guid(),
                        Reportes = c.Guid(),
                        PermisosUsers = c.Guid(),
                        PermisodeDashboard = c.Boolean(),
                        campanaPermiso = c.Boolean(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.PermisosDashes", "PermisosUsers");
            CreateIndex("dbo.PermisosDashes", "Reportes");
            CreateIndex("dbo.PermisosDashes", "Dashboard");
            CreateIndex("dbo.PermisosDashes", "Campana");
            AddForeignKey("dbo.PermisosDashes", "Reportes", "dbo.Reportes", "Id");
            AddForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users", "Id");
            AddForeignKey("dbo.PermisosDashes", "Dashboard", "dbo.Dashboards", "Id");
            AddForeignKey("dbo.PermisosDashes", "Campana", "dbo.campanas", "Id");
        }
    }
}
