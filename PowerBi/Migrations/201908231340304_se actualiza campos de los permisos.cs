namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seactualizacamposdelospermisos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PermisosDashes", "campanaPermiso", c => c.Boolean());
           // DropColumn("dbo.PermisosDashes", "PermisoCampana");
           // DropColumn("dbo.PermisosDashes", "PermisoDashboard");
           // DropColumn("dbo.PermisosDashes", "PermisDeCampana");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PermisosDashes", "PermisDeCampana", c => c.Boolean());
            AddColumn("dbo.PermisosDashes", "PermisoDashboard", c => c.Boolean());
            AddColumn("dbo.PermisosDashes", "PermisoCampana", c => c.Boolean());
            DropColumn("dbo.PermisosDashes", "campanaPermiso");
        }
    }
}
