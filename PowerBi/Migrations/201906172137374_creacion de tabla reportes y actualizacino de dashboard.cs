namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creaciondetablareportesyactualizacinodedashboard : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Dashboards", "DashboardId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Dashboards", "CampanaId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Dashboards", "ReporteId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Dashboards", "ReporteId", c => c.String());
            AlterColumn("dbo.Dashboards", "CampanaId", c => c.String());
            AlterColumn("dbo.Dashboards", "DashboardId", c => c.String());
        }
    }
}
