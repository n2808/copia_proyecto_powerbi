namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secreaforaneaduserepots : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.UserReports", "IdUsuario");
            CreateIndex("dbo.UserReports", "IdReporte");
            AddForeignKey("dbo.UserReports", "IdReporte", "dbo.Reportes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UserReports", "IdUsuario", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserReports", "IdUsuario", "dbo.Users");
            DropForeignKey("dbo.UserReports", "IdReporte", "dbo.Reportes");
            DropIndex("dbo.UserReports", new[] { "IdReporte" });
            DropIndex("dbo.UserReports", new[] { "IdUsuario" });
        }
    }
}
