namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creaciondelatablas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Campanas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reportes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        DashboardId = c.Guid(nullable: false),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserReports",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IdUsuario = c.Guid(nullable: false),
                        IdReporte = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserReports");
            DropTable("dbo.Reportes");
            DropTable("dbo.Campanas");
        }
    }
}
