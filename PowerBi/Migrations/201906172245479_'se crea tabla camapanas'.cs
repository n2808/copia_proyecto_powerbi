namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secreatablacamapanas : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Campanas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Campanas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
