namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seagragancamposdeusuarioacampana : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.campanas", "Usuario", c => c.String());
            AddColumn("dbo.campanas", "Clave", c => c.String());
            AlterColumn("dbo.Dashboards", "DashboardId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Dashboards", "DashboardId", c => c.Guid());
            DropColumn("dbo.campanas", "Clave");
            DropColumn("dbo.campanas", "Usuario");
        }
    }
}
