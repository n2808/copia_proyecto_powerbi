namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secreatablascorrectaspermisos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PermisosDashes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Campana = c.Guid(nullable: false),
                        Dashboard = c.Guid(nullable: false),
                        Reportes = c.Guid(nullable: false),
                        TipoPermiso = c.Guid(nullable: false),
                        PermisoCampana = c.Guid(nullable: false),
                        PermisoDashboard = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.campanas", t => t.Campana, cascadeDelete: false)
                .ForeignKey("dbo.Dashboards", t => t.Dashboard, cascadeDelete: false    )
                .ForeignKey("dbo.campanas", t => t.PermisoCampana, cascadeDelete: true)
                .ForeignKey("dbo.Dashboards", t => t.PermisoDashboard, cascadeDelete: false)
                .ForeignKey("dbo.Reportes", t => t.Reportes, cascadeDelete: true)
              //  .ForeignKey("dbo.PermisosTipoes", t => t.TipoPermiso, cascadeDelete: true)
                .Index(t => t.Campana)
                .Index(t => t.Dashboard)
                .Index(t => t.Reportes)
                .Index(t => t.TipoPermiso)
                .Index(t => t.PermisoCampana)
                .Index(t => t.PermisoDashboard);
            
            CreateTable(
                "dbo.PermisosTipoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Estado = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PermisosDashes", "TipoPermiso", "dbo.TipoPermisos");
            DropForeignKey("dbo.PermisosDashes", "Reportes", "dbo.Reportes");
            DropForeignKey("dbo.PermisosDashes", "PermisoDashboard", "dbo.Dashboards");
            DropForeignKey("dbo.PermisosDashes", "PermisoCampana", "dbo.campanas");
            DropForeignKey("dbo.PermisosDashes", "Dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.PermisosDashes", "Campana", "dbo.campanas");
            DropIndex("dbo.PermisosDashes", new[] { "PermisoDashboard" });
            DropIndex("dbo.PermisosDashes", new[] { "PermisoCampana" });
            DropIndex("dbo.PermisosDashes", new[] { "TipoPermiso" });
            DropIndex("dbo.PermisosDashes", new[] { "Reportes" });
            DropIndex("dbo.PermisosDashes", new[] { "Dashboard" });
            DropIndex("dbo.PermisosDashes", new[] { "Campana" });
            DropTable("dbo.PermisosTipoes");
            DropTable("dbo.PermisosDashes");
        }
    }
}
