namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seactualizanforaneadepermisosUsers : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users");
            DropIndex("dbo.PermisosDashes", new[] { "PermisosUsers" });
            AlterColumn("dbo.PermisosDashes", "PermisosUsers", c => c.Guid());
            CreateIndex("dbo.PermisosDashes", "PermisosUsers");
            AddForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users");
            DropIndex("dbo.PermisosDashes", new[] { "PermisosUsers" });
            AlterColumn("dbo.PermisosDashes", "PermisosUsers", c => c.Guid(nullable: false));
            CreateIndex("dbo.PermisosDashes", "PermisosUsers");
            AddForeignKey("dbo.PermisosDashes", "PermisosUsers", "dbo.Users", "Id", cascadeDelete: true);
        }
    }
}
