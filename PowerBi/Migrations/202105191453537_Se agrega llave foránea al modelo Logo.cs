namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregallaveforáneaalmodeloLogo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logoes", "ReporteId", c => c.Guid());
            CreateIndex("dbo.Logoes", "ReporteId");
            AddForeignKey("dbo.Logoes", "ReporteId", "dbo.Reportes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Logoes", "ReporteId", "dbo.Reportes");
            DropIndex("dbo.Logoes", new[] { "ReporteId" });
            DropColumn("dbo.Logoes", "ReporteId");
        }
    }
}
