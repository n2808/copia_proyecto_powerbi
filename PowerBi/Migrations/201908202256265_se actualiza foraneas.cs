namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seactualizaforaneas : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dashboards", "CampanaId", "dbo.campanas");
            DropForeignKey("dbo.Reportes", "DashboardId", "dbo.Dashboards");
            DropIndex("dbo.Dashboards", new[] { "CampanaId" });
            DropIndex("dbo.Reportes", new[] { "DashboardId" });
            AddColumn("dbo.campanas", "dashboard", c => c.Guid(nullable: false));
            AddColumn("dbo.Dashboards", "reportes", c => c.Guid(nullable: false));
            CreateIndex("dbo.campanas", "dashboard");
            CreateIndex("dbo.Dashboards", "reportes");
           // AddForeignKey("dbo.Dashboards", "reportes", "dbo.Reportes", "Id", cascadeDelete: false);
           // AddForeignKey("dbo.campanas", "dashboard", "dbo.Dashboards", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.campanas", "dashboard", "dbo.Dashboards");
            DropForeignKey("dbo.Dashboards", "reportes", "dbo.Reportes");
            DropIndex("dbo.Dashboards", new[] { "reportes" });
            DropIndex("dbo.campanas", new[] { "dashboard" });
            DropColumn("dbo.Dashboards", "reportes");
            DropColumn("dbo.campanas", "dashboard");
            CreateIndex("dbo.Reportes", "DashboardId");
            CreateIndex("dbo.Dashboards", "CampanaId");
            AddForeignKey("dbo.Reportes", "DashboardId", "dbo.Dashboards", "Id");
            AddForeignKey("dbo.Dashboards", "CampanaId", "dbo.campanas", "Id", cascadeDelete: true);
        }
    }
}
