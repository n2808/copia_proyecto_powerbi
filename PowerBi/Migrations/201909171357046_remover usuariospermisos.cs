namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removerusuariospermisos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserReports", "IdReporte", "dbo.Reportes");
            DropForeignKey("dbo.UserReports", "IdUsuario", "dbo.Users");
            DropIndex("dbo.UserReports", new[] { "IdUsuario" });
            DropIndex("dbo.UserReports", new[] { "IdReporte" });
            DropTable("dbo.UserReports");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserReports",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IdUsuario = c.Guid(nullable: false),
                        IdReporte = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.UserReports", "IdReporte");
            CreateIndex("dbo.UserReports", "IdUsuario");
            AddForeignKey("dbo.UserReports", "IdUsuario", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UserReports", "IdReporte", "dbo.Reportes", "Id", cascadeDelete: true);
        }
    }
}
