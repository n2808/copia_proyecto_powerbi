namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seeliminallaveforaneadereportesendashboard : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dashboards", "ReporteId", "dbo.Reportes");
            DropIndex("dbo.Dashboards", new[] { "ReporteId" });
            CreateIndex("dbo.Reportes", "DashboardId");
            AddForeignKey("dbo.Reportes", "DashboardId", "dbo.Dashboards", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reportes", "DashboardId", "dbo.Dashboards");
            DropIndex("dbo.Reportes", new[] { "DashboardId" });
            CreateIndex("dbo.Dashboards", "ReporteId");
            AddForeignKey("dbo.Dashboards", "ReporteId", "dbo.Reportes", "Id");
        }
    }
}
