USE [BDPowerBi]
GO
/****** Object:  StoredProcedure [dbo].[SP_consultarPermisos]    Script Date: 19/09/2019 16:03:13 ******/
SET ANSI_NULLS ON
GO

declare 	@Id uniqueidentifier = 'b4618ab1-1f6a-4e20-bf34-ec9a74c1abaf'

	SELECT 
	rep.Id,
		Cam.Nombre Campana,
		dash.Nombre DashBoard,
		rep.Nombre
	FROM [dbo].[PermisosDashes] pd
	left join Reportes rep on rep.Id = pd.Reportes
	join Dashboards dash on dash.Id = rep.DashboardId
	join campanas cam on cam.Id = dash.campanaId

	WHERE pd.campanaPermiso = 0 and PermisodeDashboard = 0
	AND  PermisosUsers = @Id	
	
	/*===================================================================================*/
	/*======|	LLISTA DE TODOS LOS PERMISOS POR CAMPAÑAS		|*/
	/*===================================================================================*/
	union
	SELECT 
		rep.Id , 
		Cam.Nombre Campana,
		dash.Nombre DashBoard,
		rep.Nombre
	FROM campanas cam
	join Dashboards dash on dash.campanaId = cam.Id
	join Reportes rep on rep.DashboardId = dash.Id
	join PermisosDashes pd on pd.campana = cam.Id and PermisosUsers = @Id
	where pd.campanaPermiso = 1 

	/*===================================================================================*/
	/*======|	LLISTA DE TODOS LOS PERMISOS POR DASHBOARD		|*/
	/*===================================================================================*/
	union
	SELECT 
		rep.Id , 
		Cam.Nombre Campana,
		dash.Nombre DashBoard,
		rep.Nombre
	FROM campanas cam
	join Dashboards dash on dash.campanaId = cam.Id
	join Reportes rep on rep.DashboardId = dash.Id
	join PermisosDashes pd on pd.Dashboard = dash.Id and PermisosUsers = @Id
	where  pd.[PermisodeDashboard] = 1 

